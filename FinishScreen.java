package gui;
import java.awt.EventQueue;



import javax.swing.JFrame;
import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.BorderLayout;
import javax.swing.JTextPane;
import javax.swing.JEditorPane;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import main.GameEnvironment;
import main.User;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.event.AncestorListener;
import javax.swing.event.AncestorEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;

public class FinishScreen {

	private JFrame frmFinishScreen;
	private GameEnvironment game;
	private User user;

	/**
	 * Launch the application.
	 */
	
	public FinishScreen(GameEnvironment game1, User incommingUser) {
		game = game1;
		user = incommingUser;
		initialize();
		frmFinishScreen.setVisible(true);
	}
	
	public void closeWindow() {
		frmFinishScreen.dispose();
	}
	

	/**
	 * Create the application.
	 * @param user 
	 * @param gameEnvironment 
	 */


	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmFinishScreen = new JFrame();
		frmFinishScreen.setTitle("Finish Screen");
		frmFinishScreen.getContentPane().setBackground(new Color(174, 0, 0));
		frmFinishScreen.getContentPane().setLayout(null);
		
		JButton btnCloseGame = new JButton("Close Game");
		btnCloseGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				closeWindow();
			}
		});
		btnCloseGame.setBounds(156, 200, 117, 25);
		frmFinishScreen.getContentPane().add(btnCloseGame);
		
		JLabel lblCongradulations = new JLabel("Congradulations");
		lblCongradulations.setBounds(151, 12, 141, 15);
		lblCongradulations.setForeground(new Color(249, 240, 107));
		frmFinishScreen.getContentPane().add(lblCongradulations);
		
		JLabel lblTeamName = new JLabel(game.getTeamName() + "!");
		lblTeamName.setBounds(115, 23, 188, 25);
		lblTeamName.setForeground(new Color(249, 240, 107));
		lblTeamName.setHorizontalAlignment(SwingConstants.CENTER);
		frmFinishScreen.getContentPane().add(lblTeamName);
		
		JTextArea txtrYouWon = new JTextArea();
		txtrYouWon.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent evt) {
				int diff;
				if (game.getDificulty().equalsIgnoreCase("Easy")) {
					diff = 100;
				} else {
					diff = 50;
				}
			txtrYouWon.setText("Thank You For Playing Quiditch Manager For " + game.getCurrentWeek() + " Weeks" + "!" +  "\n" + "You Won " + game.getTeamWins() + " games." + "\nYou Lost "
			+ game.getTeamLosses() + " Games" + "\nYour Win Percentage Is " + ((game.getTeamWins() * 100)/ game.getSeasonLength()) + "%" + "\nYour Point Tally For The Season Is " + (game.getTeamPoints())
				+ "\nYou Finished With " + game.getGalleons() + " Galleons.\nAnd Therefore Made A Gain/Loss Of " + (game.getGalleons()-diff));
			txtrYouWon.setEditable(false);
			}
			
		});
		
		
		txtrYouWon.setText("YOU WON ()\nYOU LOST ()\nWIN PERCENT\nTHANK YOU FOR PLAYING\n");
		txtrYouWon.setBounds(42, 49, 356, 108);
		frmFinishScreen.getContentPane().add(txtrYouWon);
		frmFinishScreen.setBounds(100, 100, 450, 300);
		frmFinishScreen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(FinishScreen.class.getResource("/images/done1.jpg")));
		label.setBounds(-269, -165, 1249, 600);
		frmFinishScreen.getContentPane().add(label);
	}
}
