package main;

public class IlleagalArgumentException extends Exception {
	
	public IlleagalArgumentException(String message) {
		super(message);
	}

}
