package gui;
import java.awt.EventQueue;





import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import main.Athlete;
import main.GameEnvironment;
import main.Item;
import main.User;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ClubScreen {

	private JFrame frmClubscreenquiditchManager;
	private JTextField textField;
	private GameEnvironment game;
	private User user;
	private Athlete athlete;
	private boolean isTypeAthlete;
	private int sellPrice;
	private Item item;
	/**
	 * Launch the application.
	 */

	/**
	 * Create the application.
	 */
	public ClubScreen(GameEnvironment game1, User incommingUser) {
		game = game1;
		user = incommingUser;
		initialize();
		frmClubscreenquiditchManager.setVisible(true);
	}
	
	public void closeWindow() {
		frmClubscreenquiditchManager.dispose();
	}
	
	public void backtomain() {
		game.closeclubScreen(this);
	}


	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmClubscreenquiditchManager = new JFrame();
		frmClubscreenquiditchManager.setTitle("ClubScreen (Quiditch Manager)");
		frmClubscreenquiditchManager.getContentPane().setBackground(new Color(174, 0, 0));
		frmClubscreenquiditchManager.setBounds(100, 100, 450, 300);
		frmClubscreenquiditchManager.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmClubscreenquiditchManager.getContentPane().setLayout(null);
		
		JTextArea productInfoText = new JTextArea();
		productInfoText.setBounds(180, 118, 248, 79);
		productInfoText.setEditable(false);
		frmClubscreenquiditchManager.getContentPane().add(productInfoText);
		
		JTextArea athleteinfotext = new JTextArea();
		athleteinfotext.setEditable(false);
		athleteinfotext.setBounds(12, 82, 146, 133);
		frmClubscreenquiditchManager.getContentPane().add(athleteinfotext);
		
		JButton btnBackToMenu = new JButton("Back To Menu");
		btnBackToMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int count = 0;
				for (Athlete athlete : user.getAthletes()) {
					if (athlete.getStatus().equalsIgnoreCase("Starter")) {
						count ++; } }
				
					if(count != 4) {
						JOptionPane.showMessageDialog(btnBackToMenu, "You must Have 4 Starters, You currently have " + count);
						} else {
							backtomain();
						}
				} 

		});
		btnBackToMenu.setFont(new Font("Dialog", Font.BOLD, 10));
		btnBackToMenu.setBounds(12, 12, 111, 25);
		frmClubscreenquiditchManager.getContentPane().add(btnBackToMenu);
		
		textField = new JTextField();
		textField.setText(game.getTeamName());
		textField.setEditable(false);
		textField.setHorizontalAlignment(SwingConstants.CENTER);
		textField.setColumns(10);
		textField.setBounds(180, 18, 146, 19);
		frmClubscreenquiditchManager.getContentPane().add(textField);
		
		JComboBox athleteComboBox = new JComboBox();
		athleteComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				athlete = ((Athlete) athleteComboBox.getSelectedItem());
				isTypeAthlete = true;
				athleteinfotext.setText(athleteComboBox.getSelectedItem() + "\n" + athlete.getDescription());
				athlete = ((Athlete) athleteComboBox.getSelectedItem());
			}
		});
		athleteComboBox.setToolTipText("");
		athleteComboBox.setMaximumRowCount(7);
		athleteComboBox.setBounds(12, 54, 146, 24);
		frmClubscreenquiditchManager.getContentPane().add(athleteComboBox);
		
		for (Athlete athlete : user.getAthletes()) {
			athleteComboBox.addItem(athlete);
		}
		
		JButton btnSwapStatus = new JButton("Swap Status");
		btnSwapStatus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				athlete.cangeStatus(athlete);
				athleteinfotext.setText(athlete.getName() + "\n" + athlete.getDescription());
				
			}
		});
		btnSwapStatus.setBounds(12, 227, 130, 25);
		frmClubscreenquiditchManager.getContentPane().add(btnSwapStatus);
		
		JLabel lblPlayersInventory = new JLabel("Current Inventory");
		lblPlayersInventory.setForeground(new Color(0, 0, 0));
		lblPlayersInventory.setBounds(180, 59, 130, 15);
		frmClubscreenquiditchManager.getContentPane().add(lblPlayersInventory);
		
		JComboBox<Item> InventoryComboBox = new JComboBox();
		InventoryComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				item = ((Item) InventoryComboBox.getSelectedItem());
				productInfoText.setText("Currently Selected: " + InventoryComboBox.getSelectedItem());
			}
		});
		InventoryComboBox.setBounds(180, 82, 168, 25);
		frmClubscreenquiditchManager.getContentPane().add(InventoryComboBox);
		
		
		for (Item item1 : user.getItems()) {
			InventoryComboBox.addItem(item1);
		}
		
		JLabel lblAthelets = new JLabel("Athelets");
		lblAthelets.setForeground(new Color(0, 0, 0));
		lblAthelets.setBounds(12, 39, 130, 15);
		frmClubscreenquiditchManager.getContentPane().add(lblAthelets);
		
		
		JButton btnApplyToAthlete = new JButton("Apply To Athlete");
		btnApplyToAthlete.setBounds(180, 201, 168, 25);
		frmClubscreenquiditchManager.getContentPane().add(btnApplyToAthlete);
		btnApplyToAthlete.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
			if (athlete == null ) {
				JOptionPane.showMessageDialog(btnApplyToAthlete, "Please select an Athlete.");
			} else {
				if (item == null) {
					JOptionPane.showMessageDialog(btnApplyToAthlete, "Please select an Item.");
				}else {
					int indexItem = user.getItems().indexOf(item);
					int indexAthlete = user.getAthletes().indexOf(athlete);
					int choice = JOptionPane.showConfirmDialog(btnApplyToAthlete, "Are you sure you want add a "+ user.getItems().get(indexItem).getName() + " of " + user.getItems().get(indexItem).getStatIncrease() + " to " + user.getAthletes().get(indexAthlete).getName() + "?", "Confirm", JOptionPane.OK_CANCEL_OPTION);
					if (choice == JOptionPane.OK_OPTION) {
						user.getItems().get(indexItem).useItem(athlete, user);
						InventoryComboBox.removeItem(InventoryComboBox.getSelectedItem());
						JOptionPane.showMessageDialog(btnApplyToAthlete, "Successfully Used!");
						athleteinfotext.setText(athlete.getName() + "\n" + athlete.getDescription());
					}
				}
			}
		}
		
	});
		
		JButton btnRegenerate = new JButton("Regenerate Stam");
		btnRegenerate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				athlete.setStam(100);
				athleteinfotext.setText(athlete.getName() + "\n" + athlete.getDescription());
				
			}
		});
		btnRegenerate.setBounds(180, 238, 168, 25);
		frmClubscreenquiditchManager.getContentPane().add(btnRegenerate);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(ClubScreen.class.getResource("/images/test3.jpg")));
		label.setBounds(-904, -182, 2000, 600);
		frmClubscreenquiditchManager.getContentPane().add(label);
		
	}
}

