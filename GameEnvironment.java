package main;
import java.util.ArrayList;


import java.util.List;

import javax.swing.JOptionPane;

import gui.ChooseTeamScreen;
import gui.ClubScreen;
import gui.FinishScreen;
import gui.GameScreen;
import gui.MainScreen;
import gui.MarketScreen;
import gui.SellScreen;
import gui.SetupScreen;
import gui.StadiumScreen;
/**
 * Environment which holds the state of the game for the different screens.
 *
 */
public class GameEnvironment {
	/**
	   * The name of the users team.
	   */
	private User user;
	
	private GameMechanics mechanics;
				
	private String teamName; 
	/**
	   * The length of the game in weeks. Between 5 and 15 weeks.
	   */
	private int seasonLength = 0;
	/**
	 * The difficulty of the game.
	 */
	private String dificulty = "easy";
	/**
	 * The difficulty of the game.
	 */
	private Shop shop;
	
	private boolean matchDone;
	
	  
	private int currentWeek = 1;
	/**
	 * The amount of galleons (currency) the player has.
	 */
	private int galleons;
	
	private boolean getResult;
	
	private int points;
	
	public static List<Athlete> athletes = new ArrayList<Athlete>();

	 private static List<String> bought = new ArrayList<String>();
	 
		private static List<User> enemyPlayers = new ArrayList<User>();

	/**
	   * The amount of weeks remaining in season given by current week - season length.
	   */
	
	private int remainingWeeks;
	
	private List<Athlete> opponentTeam1;
	private List<Athlete> opponentTeam2;
	private List<Athlete> opponentTeam3;

	private ArrayList<Athlete> enemyTeam;

	private String teamName1;

	private String teamName3;

	private String teamName2;

	private String selectedOpponentName;
	
	private int teamWins;

	private String selectedMode;
	
	private int teamLosses;
	

	
	
	public GameEnvironment() {
		for(int i = 0; i < 24; i++) {
			Athlete athlete = new Athlete();
			while(athletes.contains(athlete.getName())) {
				athlete = new Athlete();
			}
		}
		 Athlete athlete1 = new Athlete("Alex Kelly", 91, 31, 80, 90, 100, "Attacker", "", "Base");
		 addAthlete(athlete1);
	     Athlete athlete2 = new Athlete("Sam Snell", 28, 92, 80, 90, 100, "Defender", "", "Base");
		 addAthlete(athlete2);
	     Athlete athlete3 = new Athlete("Harry Potter", 99, 10, 99, 93, 100, "Attacker", "", "Base");
	     addAthlete(athlete3);
	     Athlete athlete4 = new Athlete("Ron Weasly", 22, 75, 47, 60, 100, "Defender", "", "Base");
	     addAthlete(athlete4);
	     Athlete athlete5 = new Athlete("Draco Malfoy", 71, 13, 55, 61, 100, "Attacker", "", "Base");
	     addAthlete(athlete5);
	     Athlete athlete6 = new Athlete("Hagrid", 45, 50, 56, 53, 100, "Defender", "", "Base");
	     addAthlete(athlete6);
	     Athlete athlete7 = new Athlete("Dobby The Elf", 25, 41, 33, 37, 100, "Defender", "", "Base");
	     addAthlete(athlete7);
	     Athlete athlete9 = new Athlete("Dumbledore", 10, 30, 12, 13, 100, "Defender", "", "Base");
	     addAthlete(athlete9);
	     Athlete athlete8 = new Athlete("Garaeth Walters", 57, 33, 56, 54, 100, "Attacker", "", "Base");
	     addAthlete(athlete8);
	     Athlete athlete11 = new Athlete("Tony Sissons", 16, 57, 32, 45, 100, "Defender", "", "Base");
	     addAthlete(athlete11);
	     Athlete athlete12 = new Athlete("Able Tasman", 74, 22, 33, 67, 100, "Attacker", "", "Base");
	     addAthlete(athlete12);
	     Athlete  athlete13 = new Athlete("Cosmo Graham" , 30, 40, 0, 35, 100, "Defender", ",", "Base Muggle");
	 	 addAthlete(athlete13);
	 	 Athlete athlete14 = new Athlete("Ida Samuels" , 82, 52, 79, 71, 100, "Attacker", " ", "Base");
	 	 addAthlete(athlete14);
	 	 Athlete athlete15 = new Athlete("Loreen Peel" , 62, 86, 90, 79, 100, "Defender", "", "Base");
	 	 addAthlete(athlete15);
	 	 Athlete athlete16 = new Athlete("Ada Ashworth" , 74, 74, 75, 74, 100, "Attacker", "", "Base");
	 	 addAthlete(athlete16);
	 	 Athlete athlete17 = new Athlete("Gina Bonney" , 75, 67, 93, 78, 100, "Attacker", "", "Base");
	 	 addAthlete(athlete17);
	 	 Athlete athlete18 = new Athlete("Anne Wright" , 45, 75, 75, 65, 100, "Defender", "", "Base");
	 	 addAthlete(athlete18);
	 	 Athlete athlete19 =  new Athlete("Winter Blakeley" , 80, 61, 69, 70, 100, "Attacker", "", "Base");
	 	 addAthlete(athlete19);
	 	 Athlete athlete20 = new  Athlete("Silvester Dustin" , 65, 85, 70, 73, 100, "Defender", "", "Base");
	 	 addAthlete(athlete20);
	 	 Athlete athlete21 = new Athlete("Derick Bentley" , 79, 59, 87, 75, 100, "Attacker", "", "Base");
	 	 addAthlete(athlete21);
	 	 Athlete athlete22 = new Athlete("LeBron James" , 96, 75, 65, 79, 100, "Attacker", "", "Base");
	 	 addAthlete(athlete22);
	 	 Athlete athlete23 = new Athlete("Leo Messi" , 72, 99, 76, 82, 100, "Defender", "", "Base");
	 	 addAthlete(athlete23);
	 	 Athlete athlete24 =  new Athlete("Megainrat Nickelby" , 95, 95, 95, 95, 100, "Defender", "", "Base");
	 	 addAthlete(athlete24);
	 	 Athlete athlete25 = new  Athlete("Jonathon Flowersounds" , 5, 5, 5, 5, 100, "Attacker", "", "Base");
	 	 addAthlete(athlete25);
	 	Athlete athlete26 = new  Athlete("Emma Crowe" , 91, 31, 80, 90, 100, "Attacker", "", "Base");
	 	 addAthlete(athlete26);
	 	createOpponent1();
	 	createOpponent2();
	 	createOpponent3();


		
	}
	
	/*public Team getTeam() {
		return this.teamName;
	}*/
	
	/*players = game.getTeam().getPlayers();
	for (Player player : players) {
		if (player.getName().equalsIgnoreCase("alex")) {
			player.addBroomsAthletetick();
		}Athlete
	}*/
	/**
	   * Sets the team name only as long as it's a string with a length between 3 and 15 characters.
	   */

	public void setTeamName(String teamName) throws IlleagalArgumentException {
		if(teamName.length() >= 3 && teamName.length() <= 15) {
			this.teamName = teamName;
		} else {
			JOptionPane.showMessageDialog(null, "Team name must be between 3 and 15 characters");
			throw new IlleagalArgumentException("Invalid team name length.");
		}
	}
	
	public void increaseTies() {
		points +=1;
	}
	
	public void increaseWins() {
		teamWins ++;
		points += 3;
	}
	
	public void increaseLosses() {
		teamLosses ++;
	}
	public int getTeamPoints() {
		return points;
	}
	public int getTeamLosses() {
		return teamLosses;
	}
	/**
	   * Getter method to get the team name.
	   */
	
	public String getTeamName() {
		return teamName;
	}
	
	public List<Athlete> getAthletes() {
		  return athletes;
	  }
	
	public void increaseGalForWin() {
		if(this.getDificulty().equalsIgnoreCase("Easy")) {
			galleons += 25;
		} else {
			galleons += 10;
		}
	}
		
		
	public void increaseGalForTie() {
		if(this.getDificulty().equalsIgnoreCase("easy")) {
				galleons += 13;
				this.getGalleons();
			} else {
				galleons += 5;
				this.getGalleons();
			}
	}
		
	
	
	 public void removeEnemyUser() {
		  int size = enemyPlayers.size();
		  for (int i=0; i < size; i++) {
			  enemyPlayers.remove(0);
		  }
	  }
	
	 public List<User>  getEnemyPlayers() {
		  return enemyPlayers;
	  }
	  
	 public void addEnemyUser(User opposingUser) {
		  enemyPlayers.add(opposingUser);
	  }
	
	/**
	   * Sets the length of the season as anathlete int.
	   */
	
	public void setSeasonLength(int weeks) {
		this.seasonLength = weeks;
	}
	
	  public List<String> getBought() {
		  return bought;
	  }
	  
	  public void addBought(String button) {
		  bought.add(button);
	  }
	  /**20
	   * Removes an item/bought throughout the day
	   */
	  public void removeBought() {
		  int size = bought.size();
		  for (int i = 0; i < size; i++) {
			  bought.remove(0);
		  }
	  }
	  
	  
	  
	  public boolean getMatchDone() {
		  return matchDone;
	  }
	  
	 
	  
	  public void teamWins()
	  {
		  this.teamWins += 1;
	  }
	  
	  public int getTeamWins()
	  {
		  return teamWins;
	  }

	 
	  
	  public GameMechanics getMech() {
		  return mechanics;
	  }
	  
	  public Shop getShop() {
		  return shop;
	  }
	  
	  
	/**
	   * Getter method to get the season length.
	   */
	
	public int getSeasonLength() {
		return seasonLength;
	}
	
	/**
	   * Sets the difficulty to either 'Easy' or 'Hard'.
	   */
	
	public void setDifficulty(String level) {
		this.dificulty = level;
	}
	
	/**
	   * Getter method to get the Difficulty.
	   */
	
	public String getDificulty() {
		return dificulty;
	}
	
	public static void addAthlete(Athlete athlete) {
		athletes.add(athlete);
	}
	
	public User getUser() {
		return user;
	}
	
	public void setuser(User user1) {
		user = user1;
	}
	/**
	   * Getter method to get the current week in the season.
	   */
	
	public int getCurrentWeek() {
		return currentWeek;
	}
	/** 
	 * Increases the week by 1.
	 * @return
	 */
	
	public int increaseWeek() {
		return currentWeek ++;	
	}
	/**
	   * Getter method to get the reamining weeks left in the season from season length minus the current week.
	   */
	
	
	public int getRemainingWeeks() {
		return seasonLength - currentWeek;
	}
	
	/**
	   * Sets the current balance of galleons as an int.
	   */
	
	public void setGalleons(int gal) {
		galleons = gal;
	}
	
	public void setShop(Shop shop1) {
		shop = shop1;
	}
	
	/**
	   * Getter method to get the current balance of galleons.
	   */
	
	public int getGalleons() {
		return galleons;
	}
	
	/**
	   * Opens the SetupScreen Window.
	   */
	
	public List<Athlete> createEnemyTeam()
	{
		this.enemyTeam = new ArrayList<Athlete>();
		for(int i=0; i <=3; i++) {
			Athlete athlete = new Athlete();
			athlete.createEnemyAthlete();
			enemyTeam.add(athlete);
			}
		return enemyTeam;
	}
  
  public void createOpponent1()
	{
		this.opponentTeam1 = createEnemyTeam();
		this.teamName1 = "Team 1";
	}
  public void createOpponent2()
	{
		this.opponentTeam2 = createEnemyTeam();
		this.teamName2 = "Team 2";
	}
  
  public void createOpponent3()
	{
		this.opponentTeam3 = createEnemyTeam();
		this.teamName3 = "Team 3";
	}
  
  public List<Athlete> getOpponent1()
  {
	  return opponentTeam1;
  }
  
  public List<Athlete> getOpponent2()
  {
	  return opponentTeam2;
  }
  
  public List<Athlete> getOpponent3()
  {
	  return opponentTeam3;
  }
  
  public String getOpponent1Name()
  {
	  return teamName1;
  }
  
  public String getOpponent2Name()
  {
	  return teamName2;
  }
  
  public String getOpponent3Name()
  {
	  return teamName3;
  }
	
  public void setSelectedOpponentName(String selectedOpponentName)
	  {
		  this.selectedOpponentName = selectedOpponentName;
	  }
	  
  public String getSelectedOpponentName()
	  {
		  return selectedOpponentName;
	  }
  
  public List<Athlete> getSelectedOpponent(String selectedOpponentName) 
  {
	  List<Athlete> selectedOpponent = null;
	  if (selectedOpponentName == teamName1)
	  {
		  selectedOpponent = opponentTeam1;
	  }
	  if (selectedOpponentName == teamName2)
	  {
		  selectedOpponent = opponentTeam2;
	  }
	  if (selectedOpponentName == teamName3)
	  {
		  selectedOpponent = opponentTeam3;
	  }
	  return selectedOpponent;
  }
  
  
  public int getEnemyTeamOffense(List<Athlete> enemyTeam)
	{
		int teamOffense = 0;
		for (Athlete enemy: enemyTeam)
		{
			teamOffense += enemy.getOffense();
		}
		teamOffense = teamOffense / enemyTeam.size();
		return teamOffense;
	}
	
	public int getEnemyTeamDefense(List<Athlete> enemyTeam)
	{
		int teamDefense = 0;
		for (Athlete enemy: enemyTeam)
		{
			teamDefense += enemy.getDefense();
		}
		teamDefense = teamDefense / enemyTeam.size();
		return teamDefense;
	}
	
	public int getEnemyTeamMagic(List<Athlete> enemyTeam)
	{
		int teamMagic = 0;
		for (Athlete enemy: enemyTeam)
		{
			teamMagic += enemy.getMagic();
		}
		teamMagic = teamMagic / enemyTeam.size();
		return teamMagic;
	}
	
	public int getEnemyTeamStamina(List<Athlete> enemyTeam)
	{
		int teamStamina = 0;
		for (Athlete enemy: enemyTeam)
		{
			teamStamina += enemy.getStamina();
		}
		teamStamina = teamStamina / enemyTeam.size();
		return teamStamina;
	}

	
	public void setSelectedMode(String selectedMode) {
		this.selectedMode = selectedMode;
		
	}
	
	public String getSelectedMode() {
		return selectedMode;
		
	}

	
	
	public void launchsetupScreen() {
		SetupScreen newsetUp = new SetupScreen(this, user);
	}
	
	/**
	   * Closes the SetupScreen window.
	   */
	
	public void closesetupScreen(SetupScreen newsetUp) {
		newsetUp.closeWindow();
		launchteamScreen();
	}
	
	/**
	   * Opens the ChooseTeamScreen window.
	   */
	
	public void launchteamScreen() {
		ChooseTeamScreen TeamScreen = new ChooseTeamScreen(this, user);
	}

	/**
	   * Closes the ChooseTeamScreen window.
	   */
	
	public void closeteamScreen(ChooseTeamScreen TeamScreen) {
		TeamScreen.closeWindow();
		launchmainScreen();
	}
	
	/**
	   * Opens the MainScreen window.
	   */
	
	
	public void launchmainScreen() {
		MainScreen mainScreen = new MainScreen(this, user);
	}
	
	/**
	   * Opens the MarketScreen window and Closes the MainScreen window.
	   */
	
	
	public void closetogomarket(MainScreen mainScreen) {
		mainScreen.closeWindow();
		launchmarketScreen();
		
	}
	
	/**
	   * Closes the MarketScreen window and opens the MainScreen window.
	   */
	
	
	public void closemarketScreen(MarketScreen marketScreen) {
		for(int i =0; i < 3; i++) {
			Athlete athlete = new Athlete();
			this.getShop().getAthletes().remove(athlete);
			
			Item item = new Item(this.getDificulty());
			this.getShop().getItems().remove(item);	
		}
		marketScreen.closeWindow();
		launchmainScreen();
	}

	/**
	   * Opens the MarketScreen window
	   */
	
	
	public void launchmarketScreen() {
		MarketScreen marketScreen = new MarketScreen(this, user);
	}
	
	/**
	   * Closes the MainScreen window and opens the ClubScreen.
	   */
	
	
	public void closetogoclub(MainScreen mainScreen) {
		mainScreen.closeWindow();
		launchclubScreen();
	}
	
	/**
	   * Opens the ClubScreeb window.
	   */
	
	
	public void launchclubScreen() {
		ClubScreen clubScreen = new ClubScreen(this, user);
	}
	
	/**
	   * Closes the ClubScreen window and opens the MainScreen window.
	   */
	
	
	public void closeclubScreen(ClubScreen clubScreen) {
		clubScreen.closeWindow();
		launchmainScreen();
	}
	
	/**
	   * Closes the MainScreen window and opens the StadiumScreen window.
	   */
	
	
	public void closetogostadium(MainScreen mainScreen) {
		mainScreen.closeWindow();
		launchstadiumScreen();
	}
	
	/**
	   * Opens the StadiumScreen window.
	   */
	
	public void launchstadiumScreen() {
		StadiumScreen stadiumScreen = new StadiumScreen(this, user);
	}
	
	/**
	   * Closes the StadiumScreen window and opens the MainScreen window.
	   */
	
	
	public void closestadiumScreentoMain(StadiumScreen stadiumScreen) {
		stadiumScreen.closeWindow();
		launchmainScreen();
	}
	
	/**
	   * Closes the StadiumScreen window and opens the GameScreen window.
	   */
	
	
	public void closetogogame(StadiumScreen stadiumScreen) {
		stadiumScreen.closeWindow();
		launchgameScreen();
	}
	
	/**
	   * Opens the GameScreen window.
	   */
	
	
	public void launchgameScreen() {
		GameScreen gameScreen = new GameScreen(this, user);
	}
	
	/**
	   * Closes the GameScreen window and opens the MainScreen window.
	   */
	
	
	public void closegameScreen(GameScreen gameScreen) {
		gameScreen.closeWindow();
		launchmainScreen();
	}
	
	/**
	   * Closes the MainScreen window and opens the FinishScreen window.
	   */
	
	
	public void closetoFinish(MainScreen mainScreen) {
		mainScreen.closeWindow();
		launchfinishScreen();
	}
	
	/**
	   * Opens the FinishScreen window.
	   */
	
	
	public void launchfinishScreen() {
		FinishScreen finishScreen = new FinishScreen(this, user);
	}
	
	
	
	/**
	   * Main Method.
	   */
	


	
	public static void main(String[] args) {
		GameEnvironment game = new GameEnvironment();
		game.launchsetupScreen();
		MainScreen mainFrame = new MainScreen();
	}

	public void closToGoSellScreen(MarketScreen marketScreen) {
		marketScreen.closeWindow();
		launchSellScreen();
		
	}

	public void launchSellScreen() {
		SellScreen sellScreen = new SellScreen(this, user);
		
	}
	
	public void closeSellScreen(SellScreen sellScreen) {
		sellScreen.closeWindow();
		launchmainScreen();
	}
	
	public void closeSelltoMarket(SellScreen sellScreen) {
		sellScreen.closeWindow();
		launchmarketScreen();
	}

	
	
}

