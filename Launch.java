package main;

public class Launch {
	
	/**
	 * 
	 * Authors:
	 * Alexander Kelly, Samuel Snell
	 * This class launches the game.
	 * 
	 * @param args``
	 */
	
	public static void main(String[] args) {
		User user = new User();
		GameEnvironment game = new GameEnvironment();
		Shop shop = new Shop();
		game.setuser(user);
		game.setShop(shop);
		game.launchsetupScreen();
		
	}

}