package gui;

import java.awt.EventQueue;






import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;

import main.Athlete;
import main.GameEnvironment;
import main.User;

import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JList;
import javax.swing.JOptionPane;

public class ChooseTeamScreen {

	private JFrame frmChooseTeamScreen;
	private GameEnvironment game;
	private Athlete athlete;
	private User user;
	Random random =  new Random();
	int randomIndex;
	private JButton button;
	private JTextField textField;
	private JTextField nameText;
	/**
	 * Launch the application.
	 */
	/**@wbp.parser.constructor
	 * Create the application.
	 */
	public ChooseTeamScreen(GameEnvironment game1, User incomingUser) {
		game = game1;
		user = incomingUser;
		randomIndex = random.nextInt(10);
		initialize();
		frmChooseTeamScreen.setVisible(true);
	}
	
	public void closeWindow() {
		frmChooseTeamScreen.dispose();
	}
	
	public void finishedWindow() {
		game.closeteamScreen(this);
	}
	
	public ChooseTeamScreen() {
		initialize();
	}
	



	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		User user = new User();
		
		frmChooseTeamScreen = new JFrame();
		frmChooseTeamScreen.setBackground(new Color(255, 255, 255));
		frmChooseTeamScreen.getContentPane().setBackground(new Color(174, 0, 0));
		frmChooseTeamScreen.getContentPane().setForeground(new Color(255, 255, 255));
		frmChooseTeamScreen.setTitle("Choose Team - (Quiditch Manager)");
		frmChooseTeamScreen.setBounds(100, 100, 450, 300);
		frmChooseTeamScreen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmChooseTeamScreen.getContentPane().setLayout(null);
		
		JTextArea AthleteInfoTextPane = new JTextArea();
		AthleteInfoTextPane.setEditable(false);
		AthleteInfoTextPane.setFont(new Font("Dialog", Font.PLAIN, 10));
		AthleteInfoTextPane.setBounds(275, 40, 153, 92);
		frmChooseTeamScreen.getContentPane().add(AthleteInfoTextPane);
		
		JLabel lblChooseAthletes = new JLabel("Choose 8 Athletes");
		lblChooseAthletes.setFont(new Font("Dialog", Font.BOLD, 14));
		lblChooseAthletes.setForeground(new Color(238, 186, 47));
		lblChooseAthletes.setBounds(12, 12, 153, 15);
		frmChooseTeamScreen.getContentPane().add(lblChooseAthletes);
		
		Collections.shuffle(game.getAthletes());

		JButton AthleteOneButton = new JButton(game.getAthletes().get(randomIndex).getName());
		AthleteOneButton.setFont(new Font("Dialog", Font.BOLD, 10));
		AthleteOneButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AthleteInfoTextPane.setText("Current Athlete: " + game.getAthletes().get(randomIndex).getName() + "\n" + game.getAthletes().get(randomIndex).getDescription());
				athlete = game.getAthletes().get(randomIndex);
				button = AthleteOneButton;
				nameText.setText(game.getAthletes().get(randomIndex).getName());
			}
		});
		AthleteOneButton.setBounds(12, 59, 117, 25);
		frmChooseTeamScreen.getContentPane().add(AthleteOneButton);
		
		JButton AthleteTwoButton = new JButton(game.getAthletes().get(randomIndex + 1).getName());
		AthleteTwoButton.setFont(new Font("Dialog", Font.BOLD, 10));
		AthleteTwoButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AthleteInfoTextPane.setText("Current Athlete: " + game.getAthletes().get(randomIndex + 1).getName() + "\n" + game.getAthletes().get(randomIndex + 1).getDescription());
				athlete = game.getAthletes().get(randomIndex + 1);
				button = AthleteTwoButton;
				nameText.setText(game.getAthletes().get(randomIndex + 1).getName());

			}
		});
		AthleteTwoButton.setBounds(12, 96, 117, 25);
		frmChooseTeamScreen.getContentPane().add(AthleteTwoButton);
			
		
		JButton AthleteThreeButton = new JButton(game.getAthletes().get(randomIndex + 2).getName());
		AthleteThreeButton.setFont(new Font("Dialog", Font.BOLD, 10));
		AthleteThreeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AthleteInfoTextPane.setText("Current Athlete: " + game.getAthletes().get(randomIndex + 2).getName() + "\n" + game.getAthletes().get(randomIndex + 2).getDescription());
				athlete = game.getAthletes().get(randomIndex + 2);
				button = AthleteThreeButton;
				nameText.setText(game.getAthletes().get(randomIndex + 2).getName());

			}
		});
		AthleteThreeButton.setBounds(12, 140, 117, 25);
		frmChooseTeamScreen.getContentPane().add(AthleteThreeButton);
		
		JButton AthleteFourButton = new JButton(game.getAthletes().get(randomIndex + 3).getName());
		AthleteFourButton.setFont(new Font("Dialog", Font.BOLD, 10));
		AthleteFourButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AthleteInfoTextPane.setText("Current Athlete: " + game.getAthletes().get(randomIndex + 3).getName() + "\n" + game.getAthletes().get(randomIndex + 3).getDescription());
				athlete = game.getAthletes().get(randomIndex + 3);
				button = AthleteFourButton;
				nameText.setText(game.getAthletes().get(randomIndex + 3).getName());

			}
		});
		AthleteFourButton.setBounds(12, 178, 117, 25);
		frmChooseTeamScreen.getContentPane().add(AthleteFourButton);
		
		JButton AthleteFiveButton = new JButton(game.getAthletes().get(randomIndex + 4).getName());
		AthleteFiveButton.setFont(new Font("Dialog", Font.BOLD, 10));
		AthleteFiveButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AthleteInfoTextPane.setText("Current Athlete: " + game.getAthletes().get(randomIndex + 4).getName() + "\n" + game.getAthletes().get(randomIndex + 4).getDescription());
				athlete = game.getAthletes().get(randomIndex + 4);
				button = AthleteFiveButton;
				nameText.setText(game.getAthletes().get(randomIndex + 4).getName());

			}
		});
		AthleteFiveButton.setBounds(141, 59, 117, 25);
		frmChooseTeamScreen.getContentPane().add(AthleteFiveButton);	
		
		
		JButton AthleteSixButton = new JButton(game.getAthletes().get(randomIndex + 5).getName());
		AthleteSixButton.setFont(new Font("Dialog", Font.BOLD, 10));
		AthleteSixButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AthleteInfoTextPane.setText("Current Athlete: " + game.getAthletes().get(randomIndex + 5).getName() + "\n" + game.getAthletes().get(randomIndex + 5).getDescription());
				athlete = game.getAthletes().get(randomIndex + 5);
				button = AthleteSixButton;
				nameText.setText(game.getAthletes().get(randomIndex + 5).getName());

			}
		});
		AthleteSixButton.setBounds(141, 96, 117, 25);
		frmChooseTeamScreen.getContentPane().add(AthleteSixButton);
		
		JButton AthleteSevenButton = new JButton(game.getAthletes().get(randomIndex + 6).getName());
		AthleteSevenButton.setFont(new Font("Dialog", Font.BOLD, 10));
		AthleteSevenButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AthleteInfoTextPane.setText("Current Athlete: " + game.getAthletes().get(randomIndex + 6).getName() + "\n" + game.getAthletes().get(randomIndex + 6).getDescription());
				athlete = game.getAthletes().get(randomIndex + 6);
				button = AthleteSevenButton;
				nameText.setText(game.getAthletes().get(randomIndex + 6).getName());

			}
		});
		AthleteSevenButton.setBounds(141, 140, 117, 25);
		frmChooseTeamScreen.getContentPane().add(AthleteSevenButton);
		
		JButton AthleteEightButton = new JButton(game.getAthletes().get(randomIndex + 7).getName());
		AthleteEightButton.setFont(new Font("Dialog", Font.BOLD, 10));
		AthleteEightButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AthleteInfoTextPane.setText("Current Athlete: " + game.getAthletes().get(randomIndex + 7).getName() + "\n" + game.getAthletes().get(randomIndex + 7).getDescription());
				athlete = game.getAthletes().get(randomIndex + 7);
				button = AthleteEightButton;
				nameText.setText(game.getAthletes().get(randomIndex + 7).getName());

			}
		});
		AthleteEightButton.setBounds(141, 178, 117, 25);
		frmChooseTeamScreen.getContentPane().add(AthleteEightButton);
		
		JButton AthleteNineButton = new JButton(game.getAthletes().get(randomIndex + 8).getName());
		AthleteNineButton.setFont(new Font("Dialog", Font.BOLD, 10));
		AthleteNineButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AthleteInfoTextPane.setText("Current Athlete: " + game.getAthletes().get(randomIndex + 8).getName() + "\n" + game.getAthletes().get(randomIndex + 8).getDescription());
				athlete = game.getAthletes().get(randomIndex + 8);
				button = AthleteNineButton;
				nameText.setText(game.getAthletes().get(randomIndex + 8).getName());

			}
		});
		AthleteNineButton.setBounds(12, 218, 117, 25);
		frmChooseTeamScreen.getContentPane().add(AthleteNineButton);
		
		JButton AthleteTenButton = new JButton(game.getAthletes().get(randomIndex + 9).getName());
		AthleteTenButton.setFont(new Font("Dialog", Font.BOLD, 10));
		AthleteTenButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AthleteInfoTextPane.setText("Current Athlete: " + game.getAthletes().get(randomIndex + 9).getName() + "\n" + game.getAthletes().get(randomIndex + 9).getDescription());
				athlete = game.getAthletes().get(randomIndex + 9);
				button = AthleteTenButton;
				nameText.setText(game.getAthletes().get(randomIndex+ 9).getName());
 
			}
		});
		AthleteTenButton.setBounds(141, 215, 117, 25);
		frmChooseTeamScreen.getContentPane().add(AthleteTenButton);
		
		JLabel lblAthleteStats = new JLabel("Athlete Stats");
		lblAthleteStats.setForeground(new Color(238, 186, 47));
		lblAthleteStats.setBounds(303, 15, 98, 15);
		frmChooseTeamScreen.getContentPane().add(lblAthleteStats);
		
		JButton btnAddToTeam = new JButton("Add As Starter");
		btnAddToTeam.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int count = 1;
				for(Athlete athlete : user.getAthletes()) {
					if (athlete.getStatus().equalsIgnoreCase("Starter")) {
						count ++; }
				}
				if (count <= 4) {
					user.addAthlete(athlete);
					athlete.setStatus("Starter");
					athlete.buy(game);
					button.setEnabled(false);
					} else {
					 JOptionPane.showMessageDialog(btnAddToTeam, "You can't add more than 4 starters");
	
				}				 
			}
		});
		btnAddToTeam.setBounds(275, 139, 153, 25);
		frmChooseTeamScreen.getContentPane().add(btnAddToTeam);
		
		JButton btnRemoveFromTeam = new JButton("Remove Athlete");
		btnRemoveFromTeam.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				user.removeAthlete(athlete);
			}
		});
		btnRemoveFromTeam.setBounds(275, 202, 153, 25);
		frmChooseTeamScreen.getContentPane().add(btnRemoveFromTeam);
		
		JLabel lblStartersAnd = new JLabel("*4 Starters and 4 Reserves*");
		lblStartersAnd.setForeground(new Color(255, 255, 255));
		lblStartersAnd.setBounds(12, 30, 202, 15);
		frmChooseTeamScreen.getContentPane().add(lblStartersAnd);
		
		JButton btnAddToTeam_1 = new JButton("Add As Reserve");
		btnAddToTeam_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int count = 1;
				for(Athlete athlete : user.getAthletes()) {
					if (athlete.getStatus().equalsIgnoreCase("Reserve")) {
						count ++; }
				}
				if (count <= 4) {
					user.addAthlete(athlete);
					athlete.setStatus("Reserve");
					athlete.buy(game);
					button.setEnabled(false);
				} else {
					 JOptionPane.showMessageDialog(btnAddToTeam, "You can't add more than 4 Reserves");
				}				
			}
		});
		btnAddToTeam_1.setBounds(275, 170, 153, 25);
		frmChooseTeamScreen.getContentPane().add(btnAddToTeam_1);
		
		

		JButton btnContinue = new JButton("Continue");
		btnContinue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(user.getTeamSize() != 8) {
					JOptionPane.showMessageDialog(btnContinue, "Please Select 8 Players, You currently have " + user.getTeamSize() + " Athletes.");
				} else {
					finishedWindow();
				}
					
			}
		});
	
		btnContinue.setBounds(329, 233, 99, 25);
		frmChooseTeamScreen.getContentPane().add(btnContinue);
		
		/*textField = new JTextField();
		textField.setText(athlete.getName());
		textField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String newName = textField.getSelectedText();
				athlete.setName(newName);
				textField.setText(athlete.getName());
				AthleteInfoTextPane.setText("Current Athlete: " + athlete.getName() + "\n" + athlete.getDescription());
				
			}
		});
		textField.setBounds(141, 245, 114, 19);
		frmChooseTeamScreen.getContentPane().add(textField);
		textField.setColumns(10);*/
		
		JLabel lblChangeName = new JLabel("Change Name ->");
		lblChangeName.setForeground(new Color(238, 186, 47));
		lblChangeName.setBounds(12, 245, 117, 15);
		frmChooseTeamScreen.getContentPane().add(lblChangeName);
		
		nameText = new JTextField();
		nameText.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String newName = nameText.getText();
				athlete.setName(newName);
				String updatedName = athlete.getName();
				nameText.setText(updatedName);
				AthleteInfoTextPane.setText("Current Athlete: " + updatedName + "\n" + athlete.getDescription());
			}
		});
		nameText.setBounds(141, 245, 114, 19);
		frmChooseTeamScreen.getContentPane().add(nameText);
		nameText.setColumns(10);
		
		JLabel label = new JLabel("");
		//label.setIcon(new ImageIcon(ChooseTeamScreen.class.getResource("/images/123.jpg")));
		label.setBounds(-66, -65, 891, 400);
		frmChooseTeamScreen.getContentPane().add(label);
	 
		
	}
}
