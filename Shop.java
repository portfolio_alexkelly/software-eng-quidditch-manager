package main;

import java.util.ArrayList;
import java.util.List;

public class Shop {
	
	private List<Athlete> athletes = new ArrayList<>();
	/**
	 * Adds an Item to the list of items in the store.
	 * 
	 * @param item an item
	 */
	private List<Item> itemsList = new ArrayList<>();
	/**
	 * Adds a player to the list of players.
	 * 
	 * @param player a player
	 */
	public void addAthlete(Athlete athlete) {
		athletes.add(athlete);
	}
	public void addItem(Item item) {
		itemsList.add(item);
	}
	
	public List<Athlete> getAthletes() {
		return athletes;
	}
	
	public List<Item> getItems() {
		return itemsList;
	}
	/**
	 * 
	 * 
	 */
	public void removePlayers() {
		for (int i = 0; i < athletes.size(); i++) {
			athletes.remove(0);
		}
	}
	/**
	 * Removes the items from the list of items in the store.
	 */
	public void removeItems() {
		for (int i = 0; i < itemsList.size(); i++) {
			itemsList.remove(0);
		}

	}
	

}
