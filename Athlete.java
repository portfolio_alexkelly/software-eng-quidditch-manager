package main;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
	 *Class which holds the athlete information.
	 */

public class Athlete implements MarketMechanics, UseMechanics
{
	private Random random = new Random();
	
	private GameEnvironment game;
	/**
	 *Name of the athlete as a String.
	 */
	
	private String athleteName;
	
	/**
	 *athlete offense rating as an int.
	 */
	
	private int athleteOffense;
	
	/**
	 *athlete defense rating as an int.
	 */
	
	private int athleteDefense;
	
	/**
	 *athlete magic rating as an int.
	 */
	
	private int athleteMagic;
	
	private int diffDepedningOnCurrentWeek = 0;
	
	/**
	 *athletes overall rating as an int.
	 */
	
	private int athleteOverall;
	
	/**
	 *athlete position as a String.
	 */
	
	private String athletePosition;
	
	private String rarity;
	/**
	 *athlete's stamina rating as an int.
	 */
	
	private String description;
	
	private int price;
	
	private int athleteStamina;
	
	/**
	 *athlete's status either Starter or Reserve.
	 */
	
	private String athleteStatus;
	
	private static List<String> atheleteNames = new ArrayList<String>();
	
	private static List<String> enemyNames = new ArrayList<String>();
	
	private static List<String> athleteRarities = new ArrayList<String>();

	private int athletePrice = 0;
	
	public Athlete(String athleteName, int athleteOffense, int athleteDefense, int athleteMagic, int athleteOverall, int athleteStamina, String athletePos, String athleteStatus , String athleteRarity) {
		this.athleteName = athleteName;
		this.athleteOffense = athleteOffense;
		this.athleteDefense = athleteDefense;
		this.athleteMagic = athleteMagic;
		this.athleteOverall = athleteOverall;
		this.athletePosition = athletePos;
		this.athleteStamina = athleteStamina;
		this.athleteStatus = athleteStatus;
		this.athletePosition = athletePos;
		this.rarity = athleteRarity;
		
	}
	
	public Athlete() {
		atheleteNames.add("Lord Voldermort");
		atheleteNames.add("Lionel Messi");
		atheleteNames.add("Tiger Woods");
		atheleteNames.add("James Tail");
		atheleteNames.add("King James");
		atheleteNames.add("Norman Loft");
		atheleteNames.add("Micko Francis");
		atheleteNames.add("Olivia");
		atheleteNames.add("Mason");
	    atheleteNames.add("Alex Goldstien");
	    atheleteNames.add("Liam");
	    atheleteNames.add("Callum MP");
	    atheleteNames.add("Noah");
	    atheleteNames.add("Jordan Spieth");
	    atheleteNames.add("Tama M");
	    atheleteNames.add("Joseph");
	    atheleteNames.add("Benjamin");
	    atheleteNames.add("Cristianp Ronaldo");
	    atheleteNames.add("Morgan B");
	    atheleteNames.add("Ruby");
	    atheleteNames.add("William");
		athleteRarities.add("Common");
		athleteRarities.add("Rare");
		athleteRarities.add("Legendary");
		enemyNames.add("Aragon");
		enemyNames.add("Eowyn");
		enemyNames.add("Gandalf");
		enemyNames.add("Legolas");
		enemyNames.add("Frodo");
		enemyNames.add("Gimli");
		enemyNames.add("Arya");
		enemyNames.add("Geralt");
		enemyNames.add("Merlin");
		enemyNames.add("Smaug");
		enemyNames.add("Sauron");
		enemyNames.add("Luthien");
		createAhlete();
		
		
		
	}
	
	public boolean hasStam() {
		if(athleteStamina <= 0 ) {
			return false;
		}
		return true;
	}
	
	public void useStamina(int energyUsed)
	{
		if (this.athleteStamina - energyUsed <= 0)
		{
			this.athleteStamina = 0;
		}
		
		else
			{
				this.athleteStamina -= energyUsed;
			}
	}
	
	/**
	 *Getter method for the athlete's name.
	 */
	
	public String getName()
	{
		return athleteName;
	}
	
	
	/**
	 *Changes the athletes name.
	 */
	
	public void setName(String newName)
	{
		this.athleteName = newName;
	}
	
	/**
	 *Getter method for the offense rating.
	 * @return 
	 */
	
	public int getPrice() {
		return price;
	}
	

	
	/**
	 *Getter method for the defense rating.
	 */
	
	public int getDefense()
	{
		return athleteDefense;
	}
	
	public void setAthletePosition(String pos) {
		athletePosition = pos;
	}
	
	public String getPos() {
		return athletePosition;
	}
	
	public String getDescription() {
		description = ("Offense: " + getOffense() + "\nDefense: " + getDefense() + "\nMagic:  " + getMagic() + "\nOverAll: " + getOverall() + "\nStamina: " + getStamina() + "\nRarity: " + getRarity() 
		+ "\nStatus: " + getStatus());
		return description;  
	}
	
	/**
	 *Getter method for the Magic rating.
	 * @return 
	 */
	
	public int getOffense() {
		return athleteOffense;
	}
	public void setMagic(int stat) {
		athleteMagic = stat;
	}
	
	public void setOffense(int stat) {
		athleteOffense = stat;
	}
	
	public void setDeffense(int stat) {
		athleteDefense = stat;
	}
	
	public void setStam(int stat) {
		athleteStamina = stat;
	}
	
	public int getMagic()
	{
		return athleteMagic;
	}
	
	/**
	 *Getter method for the overall rating.
	 */
	
	public int getOverall()
	{
		return athleteOverall;
	}
	public String toString() {
		return athleteName;
	}
	
	/**athleteName
	 *Getter method for the Postion.
	 */
	
	public String getPosition()
	{
		return athletePosition;
	}
	
	/**
	 *Getter method for the Stamina rating.
	 */
	
	public int getStamina() 
	{
		return athleteStamina;
	}
	
	/**
	 *Getter method for the athlete's Status.
	 */
	
	public String getStatus()
	{
		return athleteStatus;
	}
	
	/**
	 *Sets the athlete's Status as a starter.
	 */
	
	public void setStatus(String status)
	{
		this.athleteStatus = status;
	}
	
	public void cangeStatus(Athlete athlete) {
		if(athlete.getStatus().equalsIgnoreCase("Starter")) {
			athlete.setStatus("Reserve");
		} else {
			athlete.setStatus("Starter");
		}
	}
	
	public void setOverall(int val) {
		athleteOverall = val;
	}
	
	
	/**
	 *Adds the broomstick item to a athlete.
	 */
	
	public void addBroomstick()
	{
		if(athleteDefense >= 84) 
		{
			this.athleteDefense = 99;
		}
		else
		{
			this.athleteDefense += 15;
		}
		
		if(athleteOffense >= 84) 
		{
			this.athleteOffense = 99;
		}
		else
		{
			this.athleteOffense += 15;
		}
		
		updateOverall();
	}
	
	/**
	 *Adds the uniform item to a athlete.
	 */
	
	public void addUniform() {
	
	
		if(athleteDefense >= 94) 
		{
			this.athleteDefense = 99;
		}
		else
		{
			this.athleteDefense += 5;
		} 
		
		if(athleteOffense >= 94) 
		{
			this.athleteOffense = 99;
		}
		else
		{
			this.athleteOffense += 5;
		}
		
		if(athleteMagic >= 94) 
		{
			this.athleteMagic = 99;
		}
		else
		{
			this.athleteMagic += 5;
		}
		updateOverall();
	}
	
	public void addPotion()
	{
		if(athleteMagic >= 94) 
		{
			this.athleteMagic = 99;
		}
		else
		{
			this.athleteMagic += 5;
		}
		updateOverall();
	}
	
	/**
	 *Updates the overall of a athlete by calculating average of all stats.
	 */
	
	public void updateOverall()
	{
		this.athleteOverall = (athleteDefense + athleteOffense + athleteMagic)/3;
	}
	public void setRarity(String level) {
		rarity = level;
	}
	
	public String getRarity() {
		return rarity;
	}
	
	/**
	 *Resets athlete's stamina back to 100.
	 */
	
	public void replenishStamina()
	{
		this.athleteStamina = 100;
	}
	
	public List<String> getAthelteNames() {
		return atheleteNames;
	}
	
	public List<String> getAthleteRarities() {
		return athleteRarities;
	}
	
	public List<String> getEnemyNames() {
		return enemyNames;
	}
	
	public void setDiffNum() {
		if(game.getCurrentWeek() > (game.getSeasonLength()/2)) {
			diffDepedningOnCurrentWeek = 1;
		}
		diffDepedningOnCurrentWeek = 0;
		
	}
	
	public int getDiffNum() {
		return diffDepedningOnCurrentWeek;
	}
	
	
	public void createAhlete() {
		int randomRarity = random.nextInt(getAthleteRarities().size());
		this.setRarity(getAthleteRarities().get(randomRarity));
		if (this.rarity.equalsIgnoreCase("Common")) {
			int randomName = random.nextInt(getAthelteNames().size());
			int randomOffense = random.nextInt(30,50);
			int randomDeffense = random.nextInt(30,50);
			int randomMagic = random.nextInt(30,50);
			int baseStam = 100;
			int price = random.nextInt(5,29);
			this.price = price;
			int overAll = ((randomOffense + randomDeffense + randomMagic)/3);
			String Status = "";
			String pos = "";
			String rareityType = "Common";
			this.setRarity(rareityType);
			this.setAthletePosition(pos);
			this.setStatus(Status);
			this.setOverall(overAll);
			this.setStatus("Starter");
			this.setName((getAthelteNames().get(randomName)));
			this.setOffense(randomOffense);
			this.setDeffense(randomDeffense);
			this.setMagic(randomMagic);
			this.setStam(baseStam);
			Athlete athlete = new Athlete(getAthelteNames().get(randomName), randomOffense, randomDeffense, randomMagic, overAll, baseStam, pos, Status,  rareityType);
		} else if (this.rarity.equalsIgnoreCase("Rare")) {
			int randomName = random.nextInt(getAthelteNames().size());
			int randomOffense = random.nextInt(50, 70);
			int randomDeffense = random.nextInt(50,70);
			int randomMagic = random.nextInt(50,70);
			int baseStam = 100;
			int price = random.nextInt(30,59);
			this.price = price;
			int overAll = ((randomOffense + randomDeffense + randomMagic)/3);
			String Status = "";
			String pos = "";
			String rareityType = "Rare";
			this.setRarity(rareityType);
			this.setAthletePosition(pos);
			this.setStatus(Status);
			this.setOverall(overAll);
			this.setName((getAthelteNames().get(randomName)));
			this.setOffense(randomOffense);
			this.setDeffense(randomDeffense);
			this.setMagic(randomMagic);
			this.setStam(baseStam);
			Athlete athlete = new Athlete(getAthelteNames().get(randomName), randomOffense, randomDeffense, randomMagic, overAll, baseStam, pos, Status, rareityType);
		} else if (this.rarity.equalsIgnoreCase("Legendary")) {
			int randomName = random.nextInt(getAthelteNames().size());
			int randomOffense = random.nextInt(70,99);
			int randomDeffense = random.nextInt(70,99);
			int randomMagic = random.nextInt(70,99);
			int baseStam = 100;
			int price = random.nextInt(60,99);
			this.price = price;
			int overAll = ((randomOffense + randomDeffense + randomMagic)/3);
			String Status = "";
			String pos = "";
			String rareityType = "Legendary";
			this.setRarity(rareityType);
			this.setAthletePosition(pos);
			this.setStatus(Status);
			this.setOverall(overAll);
			this.setStatus("Starter");
			this.setName((getAthelteNames().get(randomName)));
			this.setOffense(randomOffense);
			this.setDeffense(randomDeffense);
			this.setMagic(randomMagic);
			this.setStam(baseStam);
			Athlete athlete = new Athlete(getAthelteNames().get(randomName), randomOffense, randomDeffense, randomMagic, overAll, baseStam, pos, Status,  rareityType);	
		}
	}
	
	public void createEnemyAthlete() {
		if(diffDepedningOnCurrentWeek == 0) {
			int randomName = random.nextInt(getEnemyNames().size());
			int randomOffense = random.nextInt(30, 50);
			int randomDeffense = random.nextInt(30,50);
			int randomMagic = random.nextInt(30,50);
			int baseStam = 100;
			int price = random.nextInt(0,1);
			this.price = price;
			int overAll = ((randomOffense + randomDeffense + randomMagic)/3);
			String Status = "Enemy";
			String pos = "Enemy";
			String rareityType = "Enemy";
			this.setRarity(rareityType);
			this.setAthletePosition(pos);
			this.setStatus(Status);
			this.setOverall(overAll);
			this.setName((getAthelteNames().get(randomName)));
			this.setOffense(randomOffense);
			this.setDeffense(randomDeffense);
			this.setMagic(randomMagic);
			this.setStam(baseStam);
			Athlete athlete = new Athlete(getAthelteNames().get(randomName), randomOffense, randomDeffense, randomMagic, overAll, baseStam, pos, Status, rareityType);
			
		} else {
			int randomName = random.nextInt(getEnemyNames().size());
			int randomOffense = random.nextInt(50, 70);
			int randomDeffense = random.nextInt(50,70);
			int randomMagic = random.nextInt(50,70);
			int baseStam = 100;
			int price = random.nextInt(0,1);
			this.price = price;
			int overAll = ((randomOffense + randomDeffense + randomMagic)/3);
			String Status = "Enemy";
			String pos = "Enemy";
			String rareityType = "Enemy";
			this.setRarity(rareityType);
			this.setAthletePosition(pos);
			this.setStatus(Status);
			this.setOverall(overAll);
			this.setName((getAthelteNames().get(randomName)));
			this.setOffense(randomOffense);
			this.setDeffense(randomDeffense);
			this.setMagic(randomMagic);
			this.setStam(baseStam);
			Athlete athlete = new Athlete(getAthelteNames().get(randomName), randomOffense, randomDeffense, randomMagic, overAll, baseStam, pos, Status, rareityType);
			
		}
		
	}
	
	
	
	
	
	
	
	/* else {
			int randomName = random.nextInt(getAthelteNames().size());
			int randomOffense = random.nextInt(60,99);
			int randomDeffense = random.nextInt(60,99);
			int randomMagic = random.nextInt(60,99);
			int baseStam = 100;
			int overAll = ((randomOffense + randomDeffense + randomMagic)/3);
			this.setOverall(overAll);
			String Status = "";
			String pos = "";
			this.setathletePosition(pos);
			this.setStatus(Status);
			this.setOverall(overAll);
			this.setStatus("Starter");
			this.setName((getAthelteNames().get(randomName)));
			this.setOffense(randomOffense);
			this.setDeffense(randomDeffense);
			this.setMagic(randomMagic);
			this.setStam(baseStam);
			Athlete athlete = new Athlete(getAthelteNames().get(randomName), randomOffense, randomDeffense, randomMagic, overAll, baseStam, pos, Status);
		} */

	public void buy(GameEnvironment environment) {
		// TODO Auto-generated method stub
		environment.setGalleons(environment.getGalleons()- getPrice());
		environment.getUser().addAthlete(this);
		
	}

	@Override
	public void sell(GameEnvironment environment) {
		int salePrice = (int) (getPrice() * 0.75);
		environment.setGalleons(environment.getGalleons() + salePrice);
		environment.getUser().removeAthlete(this);
	}

	@Override
	public void useItem(Athlete athelte, User user) {
		// TODO Auto-generated method stub
		
	}



	

}

