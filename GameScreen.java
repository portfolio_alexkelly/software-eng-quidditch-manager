package gui;
import java.awt.EventQueue;




import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import main.Athlete;
import main.GameEnvironment;
import main.GameMechanics;
import main.User;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Random;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import java.awt.Color;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;

public class GameScreen {

	private JFrame frmGameScreen;
	private JTextField teamNameField;
	private JTextField txtOpponent;
	private JTextField teamScoreField;
	private JTextField opponentScoreField;
	private GameEnvironment game;
	private User user;
	private GameMechanics playGame;
	private Athlete athlete;
	private Random random = new Random();
	private Athlete opposingAthlete;
	private String selectedStat;
	private int level;
	/**
	 * Launch the application.
	 */

	
	public GameScreen(GameEnvironment game1, User incommingUser) {
		game = game1;
		user = incommingUser;
		initialize();
		frmGameScreen.setVisible(true);
		
		
	}
	
	public void closeWindow() {
		frmGameScreen.dispose();
	}
	
	public void backtomain() {
		game.closegameScreen(this);
	}
	

	/**
	 * Create the application.
	 * @param user 
	 * @param gameEnvironment 
	 */


	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmGameScreen = new JFrame();
		frmGameScreen.setTitle("Game Screen");
		frmGameScreen.getContentPane().setBackground(new Color(174, 0, 0));
		frmGameScreen.setBounds(100, 100, 450, 300);
		frmGameScreen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmGameScreen.getContentPane().setLayout(null);
		playGame = new GameMechanics(0, 0);
		level = 0;
		List<Athlete> opposingAthleteList = game.getSelectedOpponent(game.getSelectedOpponentName());
		opposingAthlete = opposingAthleteList.get(level);
		
		JTextArea txtAthleteStats = new JTextArea();
		txtAthleteStats.setBounds(137, 106, 114, 90);
		frmGameScreen.getContentPane().add(txtAthleteStats);
		txtAthleteStats.setEditable(false);
		
		JTextArea txtOpponentStats = new JTextArea();
		txtOpponentStats.setText(opposingAthlete.getName() + "\n" + opposingAthlete.getDescription());
		txtOpponentStats.setBounds(279, 106, 114, 90);
		frmGameScreen.getContentPane().add(txtOpponentStats);
		txtOpponentStats.setEditable(false);
		
		teamNameField = new JTextField();
		teamNameField.setText(game.getTeamName());
		teamNameField.setHorizontalAlignment(SwingConstants.CENTER);
		teamNameField.setColumns(10);
		teamNameField.setBounds(29, 28, 146, 19);
		frmGameScreen.getContentPane().add(teamNameField);
		teamNameField.setEditable(false);
		
		JLabel lblVs = new JLabel("VS");
		lblVs.setForeground(new Color(238, 186, 47));
		lblVs.setBounds(203, 43, 70, 15);
		frmGameScreen.getContentPane().add(lblVs);
		
		txtOpponent = new JTextField();
		txtOpponent.setText(game.getSelectedOpponentName());
		txtOpponent.setHorizontalAlignment(SwingConstants.CENTER);
		txtOpponent.setColumns(10);
		txtOpponent.setBounds(250, 28, 146, 19);
		frmGameScreen.getContentPane().add(txtOpponent);
		txtOpponent.setEditable(false);
		
		teamScoreField = new JTextField();
		
		teamScoreField.setHorizontalAlignment(SwingConstants.CENTER);
		teamScoreField.setText("" + playGame.getMyTeamScore());
		teamScoreField.setBounds(79, 50, 49, 19);
		frmGameScreen.getContentPane().add(teamScoreField);
		teamScoreField.setColumns(10);
		teamScoreField.setEditable(false);
		
		opponentScoreField = new JTextField();
		opponentScoreField.setText("" + playGame.getOpposingTeamScore());
		opponentScoreField.setHorizontalAlignment(SwingConstants.CENTER);
		opponentScoreField.setColumns(10);
		opponentScoreField.setBounds(301, 50, 49, 19);
		frmGameScreen.getContentPane().add(opponentScoreField);
		opponentScoreField.setEditable(false);
		
		JLabel lblChoosePlayer = new JLabel("Choose Player");
		lblChoosePlayer.setForeground(new Color(238, 186, 47));
		lblChoosePlayer.setHorizontalAlignment(SwingConstants.CENTER);
		lblChoosePlayer.setBounds(14, 93, 114, 15);
		frmGameScreen.getContentPane().add(lblChoosePlayer);
		
		JComboBox<Athlete> comboBoxChoosePlayer = new JComboBox();
		comboBoxChoosePlayer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				athlete = ((Athlete) comboBoxChoosePlayer.getSelectedItem());
				txtAthleteStats.setText(comboBoxChoosePlayer.getSelectedItem() + "\n" + athlete.getDescription());
			}
		});
		
		for (Athlete athlete : user.getAthletes()) {
			if (athlete.getStatus().equalsIgnoreCase("Starter")) {
				comboBoxChoosePlayer.addItem(athlete);
			}
		}
		
		comboBoxChoosePlayer.setMaximumRowCount(7);
		comboBoxChoosePlayer.setBounds(14, 106, 111, 30);
		frmGameScreen.getContentPane().add(comboBoxChoosePlayer);
		
		
		
		JLabel lblChooseStat = new JLabel("Choose Mode");
		lblChooseStat.setForeground(new Color(238, 186, 47));
		lblChooseStat.setHorizontalAlignment(SwingConstants.CENTER);
		lblChooseStat.setBounds(14, 148, 124, 15);
		frmGameScreen.getContentPane().add(lblChooseStat);
		
		JComboBox comboBoxChooseStat = new JComboBox();
		comboBoxChooseStat.setModel(new DefaultComboBoxModel(new String[] {"Attack", "Defense", "Snitch Mode"}));
		comboBoxChooseStat.setBounds(14, 163, 111, 30);
		frmGameScreen.getContentPane().add(comboBoxChooseStat);
		comboBoxChooseStat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String selectedMode = (String) comboBoxChooseStat.getSelectedItem();
				game.setSelectedMode(selectedMode);
				selectedStat = (String) comboBoxChooseStat.getSelectedItem();
			}
		});
	
		
		JButton btnPlay = new JButton("Play");
		btnPlay.setBounds(14, 204, 114, 30);
		frmGameScreen.getContentPane().add(btnPlay);
		btnPlay.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    	int usersCurrPlayerCount = 4;
		    	int oppsCurrPlayerCoount = 4;
		    		if (athlete == null || selectedStat == null) {
			            JOptionPane.showMessageDialog(btnPlay, "Please select an Athlete and a Stat.");
			        } else if (athlete.getStamina() <= 0) {
			            JOptionPane.showMessageDialog(btnPlay, "This player is out of stamina. Please use another player.");
			            usersCurrPlayerCount --;
			        } else if (opposingAthlete.getStamina() <= 0) {
			            if (level + 1 < opposingAthleteList.size()) {
			                opposingAthlete = opposingAthleteList.get(level + 1);
			                level++;
			                JOptionPane.showMessageDialog(btnPlay, "You defeated the opposing Player who is now out of Stamina.\nThey are now replaced by " + opposingAthlete.getName());
			                txtOpponentStats.setText(opposingAthlete.getName() + "\n" + opposingAthlete.getDescription());
			                oppsCurrPlayerCoount--;
			            } else if(oppsCurrPlayerCoount == 0) {
			            	// Handle the case when there are no more players available in the opposing team
			                JOptionPane.showMessageDialog(btnPlay, "The opposing team has no more players. Please Click Finish Game!");
			            } else {
			            	// Handle the case when there are no more players available in the opposing team
			                JOptionPane.showMessageDialog(btnPlay, "The opposing team has no more players. Please Click Finish Game!");
			            }
			        } else {
			            if (game.getSelectedMode() == "Attack") {
			                playGame.headToHeadAttack(athlete, opposingAthlete);
			                txtAthleteStats.setText(athlete.getName() + "\n" + athlete.getDescription());
			                txtOpponentStats.setText(opposingAthlete.getName() + "\n" + opposingAthlete.getDescription());
			                teamScoreField.setText("" + playGame.getMyTeamScore());
			                opponentScoreField.setText("" + playGame.getOpposingTeamScore());
			            } else if (game.getSelectedMode() == "Defense") {
			                playGame.headToHeadDefense(athlete, opposingAthlete);
			                txtAthleteStats.setText(athlete.getName() + "\n" + athlete.getDescription());
			                txtOpponentStats.setText(opposingAthlete.getName() + "\n" + opposingAthlete.getDescription());
			                teamScoreField.setText("" + playGame.getMyTeamScore());
			                opponentScoreField.setText("" + playGame.getOpposingTeamScore());
			            } else if (game.getSelectedMode() == "Snitch Mode") {
			                playGame.chanceSnitchOccurs(athlete, opposingAthlete);
			                txtAthleteStats.setText(athlete.getName() + "\n" + athlete.getDescription());
			                txtOpponentStats.setText(opposingAthlete.getName() + "\n" + opposingAthlete.getDescription());
			                teamScoreField.setText("" + playGame.getMyTeamScore());
			                opponentScoreField.setText("" + playGame.getOpposingTeamScore());
			            }
			        }
			    
		    }
		    		
		});

		// Check if all enemy players are out of stamina
        /*boolean allEnemyPlayersOutOfStamina = true;
        int oppCount = 0;
        for (Athlete enemyAthlete : opposingAthleteList) {
            if (enemyAthlete.getStamina() > 0) {
                oppCount++;
                allEnemyPlayersOutOfStamina = false;
            }
        }
        
        // Check if all player athletes are out of stamina
        boolean allPlayerAthletesOutOfStamina = true;
        int count = 0;
        for (Athlete playerAthlete : user.getAthletes()) {
            if (playerAthlete.getStamina() > 0) {
                count++;
                allPlayerAthletesOutOfStamina = false;
            }
        }
        
        // Display win/loss message if necessary
        if (allEnemyPlayersOutOfStamina) {
            if (playGame.getOpposingTeamScore() < playGame.getMyTeamScore()) {
                JOptionPane.showMessageDialog(btnPlay, "The Game is Over. Congratulations, you won!");
            } else if (playGame.getOpposingTeamScore() > playGame.getMyTeamScore()) {
                JOptionPane.showMessageDialog(btnPlay, "The Game isOver. Unlucky, you lost.");
            }
        }

        if (count == user.getAthletes().size()) {
            if (playGame.getOpposingTeamScore() < playGame.getMyTeamScore()) {
                JOpti
            }onPane.showMessageDialog(btnPlay, "The Game is Over. Congratulations, you won!");
                game.increaseWins();
            } else if (playGame.getOpposingTeamScore() > playGame.getMyTeamScore()) {
                JOptionPane.showMessageDialog(btnPlay, "The Game is Over. Unlucky, you lost.");
            }
        }*/

		
		
		JLabel lblOpponentPlayer = new JLabel("Opponent Player");
		lblOpponentPlayer.setForeground(new Color(238, 186, 47));
		lblOpponentPlayer.setBounds(275, 87, 138, 15);
		frmGameScreen.getContentPane().add(lblOpponentPlayer);
		
		
		JButton btnFinishGame = new JButton("Finish Game");
		btnFinishGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (playGame.getOpposingTeamScore() > playGame.getMyTeamScore()) {
					JOptionPane.showMessageDialog(btnPlay, "The Game isOver. Unlucky, you lost. Please Click Finish");
					game.increaseLosses();
					backtomain();
					game.createOpponent1();
					game.createOpponent2();
					game.createOpponent3();
				} else if (playGame.getOpposingTeamScore() == playGame.getMyTeamScore()){
					JOptionPane.showMessageDialog(btnPlay, "Wow A Tie. You get half the winnings. Please Click Finish");
					game.increaseGalForTie();
					game.increaseTies();
					
					backtomain();
					game.createOpponent1();
					game.createOpponent2();
					game.createOpponent3();
				} else {
					JOptionPane.showMessageDialog(btnPlay, "The Game is Over. Congratulations, you won!");
					game.increaseGalForWin();
					game.increaseWins();
					backtomain();
					game.createOpponent1();
					game.createOpponent2();
					game.createOpponent3();
				}	
			}
		});
		btnFinishGame.setBounds(279, 233, 120, 25);
		frmGameScreen.getContentPane().add(btnFinishGame);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(GameScreen.class.getResource("/images/potter2.jpg")));
		label.setBounds(-153, -31, 657, 464);
		frmGameScreen.getContentPane().add(label);
		
	}
}


/*int usersCurrPlayerCount = 4;
int oppsCurrPlayerCoount = 4;

if (usersCurrPlayerCount == 0 && oppsCurrPlayerCoount > 0) {
	playGame.incOpposingTeamScore(15 * oppsCurrPlayerCoount);
	playGame.getOpposingTeamScore();
	if (playGame.getOpposingTeamScore() > playGame.getMyTeamScore()) {
		JOptionPane.showMessageDialog(btnPlay, "The Game isOver. Unlucky, you lost. Please Click Finish");
	} else if (playGame.getOpposingTeamScore() == playGame.getMyTeamScore()){
		JOptionPane.showMessageDialog(btnPlay, "Wow A Tie. You get half the winnings. Please Click Finish");
		game.increaseGalForTie();
	} else {
		JOptionPane.showMessageDialog(btnPlay, "The Game is Over. Congratulations, you won!");
		game.increaseGalForWin();
	}
} else if (usersCurrPlayerCount > 0 && oppsCurrPlayerCoount == 0) {
	playGame.incMyTeamScore(15 *usersCurrPlayerCount );
	playGame.getMyTeamScore();
	if (playGame.getOpposingTeamScore() > playGame.getMyTeamScore()) {
		JOptionPane.showMessageDialog(btnPlay, "The Game isOver. Unlucky, you lost. Please Click Finish");
	} else if (playGame.getOpposingTeamScore() == playGame.getMyTeamScore()){
		JOptionPane.showMessageDialog(btnPlay, "Wow A Tie. You get half the winnings. Please Click Finish");
		game.increaseGalForTie();
	} else {
		JOptionPane.showMessageDialog(btnPlay, "The Game is Over. Congratulations, you won!");
		game.increaseGalForWin();
	}
} else if (usersCurrPlayerCount == 0 && oppsCurrPlayerCoount == 0) {
	if (playGame.getOpposingTeamScore() > playGame.getMyTeamScore()) {
		JOptionPane.showMessageDialog(btnPlay, "The Game isOver. Unlucky, you lost. Please Click Finish");
	} else if (playGame.getOpposingTeamScore() == playGame.getMyTeamScore()){
		JOptionPane.showMessageDialog(btnPlay, "Wow A Tie. You get half the winnings. Please Click Finish");
		game.increaseGalForTie();
	} else {
		JOptionPane.showMessageDialog(btnPlay, "The Game is Over. Congratulations, you won!");
		game.increaseGalForWin();
	}
} */

