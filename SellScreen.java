package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;

import main.Athlete;
import main.GameEnvironment;
import main.Item;
import main.User;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.BorderLayout;
import javax.swing.JTextArea;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class SellScreen {

	private JFrame SellScreen;
	private Item item;
	private User user;
	private GameEnvironment game;
	private Athlete athlete;
	private boolean isTypeAthlete;
	private int sellPrice;

	/**
	 * Launch the application.
	 */


	/**
	 * Create the application.
	 */
	public SellScreen(GameEnvironment game1, User incomingUser) {
		game = game1;
		user = incomingUser;
		initialize();
		SellScreen.setVisible(true);
	}
	
	public void closeWindow() {
		SellScreen.dispose();
	}
	
	public void closeSellScreen() {
		game.closeSellScreen(this);
		
	}
	public void closetoMarket() {
		game.closeSelltoMarket(this);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		SellScreen = new JFrame();
		SellScreen.setTitle("SellScreen");
		SellScreen.getContentPane().setBackground(new Color(174, 0, 0));
		SellScreen.getContentPane().setLayout(null);
		
		JLabel lblNewLabel_2 = new JLabel("Current Balance");
		lblNewLabel_2.setForeground(new Color(238, 186, 47));
		lblNewLabel_2.setBounds(304, 12, 124, 15);
		SellScreen.getContentPane().add(lblNewLabel_2);
		
		JTextArea CurrencyText = new JTextArea();
		CurrencyText.setText("ʛ " + game.getGalleons());
		CurrencyText.setBounds(304, 30, 114, 15);
		SellScreen.getContentPane().add(CurrencyText);
		
		JLabel lblNewLabel_2_1 = new JLabel("Owned Products");
		lblNewLabel_2_1.setForeground(new Color(249, 240, 107));
		lblNewLabel_2_1.setBounds(22, 150, 134, 15);
		SellScreen.getContentPane().add(lblNewLabel_2_1);
		
		JTextArea productInfoText = new JTextArea();
		productInfoText.setFont(new Font("Dialog", Font.PLAIN, 12));
		productInfoText.setBounds(176, 109, 252, 137);
		SellScreen.getContentPane().add(productInfoText);
		
		JLabel lblProductInfo = new JLabel("Product Info");
		lblProductInfo.setForeground(new Color(249, 240, 107));
		lblProductInfo.setBounds(254, 80, 107, 15);
		SellScreen.getContentPane().add(lblProductInfo);
		
		JComboBox comboBox = new JComboBox();
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(comboBox.getSelectedItem() instanceof Athlete) {
					isTypeAthlete = true;
					athlete = ((Athlete) comboBox.getSelectedItem());
					sellPrice = ((int) (0.75 * athlete.getPrice()));
					productInfoText.setText("Currently Selected: " + comboBox.getSelectedItem() + "\n" + athlete.getDescription() + "\nSell Price: " 
					+ sellPrice + " Galleons");
				} else {
					isTypeAthlete = false;
					item = ((Item) comboBox.getSelectedItem());
					sellPrice = ((int) (0.75 * item.getPrice()));
					productInfoText.setText("Currently Selected: " + comboBox.getSelectedItem() + "\n" + item.getDescription() + "\nSell Price: " 
					+ sellPrice + " Galleons");
				}
			}
		});
		comboBox.setBounds(22, 169, 124, 24);
		SellScreen.getContentPane().add(comboBox);
		
		for (Athlete athlete : user.getAthletes()) {
			comboBox.addItem(athlete);
		}
		
		for (Item item : user.getItems()) {
			comboBox.addItem(item);
		}
		
		JButton SellButton = new JButton("Sell Product");
		SellButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(athlete == null && item == null) {
					JOptionPane.showMessageDialog(SellButton, "Please select a Item or Athlete to Sell.");
				} else {
					if (isTypeAthlete == true && user.getAthletes().size() == 8) {
						JOptionPane.showMessageDialog(SellButton, "Sorry, you need at least 8 athletes to play.");
				} else {
					int decisionToSell = JOptionPane.showConfirmDialog(SellButton, "Press 'Ok' to confirm your sale. You will recieve " + sellPrice + " Galleons", "Checkout", JOptionPane.OK_CANCEL_OPTION);
					if (decisionToSell == JOptionPane.OK_OPTION) {
						if(isTypeAthlete == true) {
							athlete.sell(game);
						} else {
							item.sell(game);
						}
						comboBox.removeItem(comboBox.getSelectedItem());
						JOptionPane.showMessageDialog(SellButton, "Congradulations, You Made A Sale!");
						CurrencyText.setText("ʛ " + game.getGalleons());
						}
				}
				
			}
			}
		});
		SellButton.setBounds(22, 221, 124, 25);
		SellScreen.getContentPane().add(SellButton);
		
		JButton btnToMarket = new JButton("To Market");
		btnToMarket.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				closetoMarket();
			}
		});
		btnToMarket.setBounds(12, 7, 134, 25);
		SellScreen.getContentPane().add(btnToMarket);
		
		JButton btnToMainMenu = new JButton("To Main Menu");
		btnToMainMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				closeSellScreen();
			}
		});
		btnToMainMenu.setBounds(12, 49, 134, 25);
		SellScreen.getContentPane().add(btnToMainMenu);
		SellScreen.setBounds(100, 100, 450, 300);
		SellScreen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(SellScreen.class.getResource("/images/sell.jpg")));
		label.setBounds(-149, -150, 657, 464);
		SellScreen.getContentPane().add(label);
		
		
		
	}

}
