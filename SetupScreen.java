package gui;
import java.awt.EventQueue;




import javax.swing.JFrame;
import java.awt.GridLayout;
import java.awt.CardLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.BorderLayout;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JComboBox;
import javax.swing.DropMode;
import javax.swing.ImageIcon;
import javax.swing.JScrollBar;
import javax.swing.JSlider;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JToggleButton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import java.awt.Font;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import java.awt.Color;
import java.awt.Window.Type;
import javax.swing.JRadioButton;
import javax.swing.event.ChangeListener;

import main.GameEnvironment;
import main.IlleagalArgumentException;
import main.Item;
import main.User;

import javax.swing.event.ChangeEvent;
import javax.swing.JCheckBox;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;

public class SetupScreen {

	private JFrame frmSetupscreen;
	private JTextField txtChooseTeamName;
	private JButton btnChoosePlayers;
	private JLabel lblChooseSeasonLength;
	private JTextField txtStartingGalleonsEasy;
	private JTextField txtGalleonsPerWinEasy;
	private JTextField txtOpponentStatsEasy;
	private JTextField txtStartingGalleonsHard;
	private JTextField txtGalleonsPerWinHard;
	private JTextField txtOpponentStatsHard;
	private MainScreen mainFrame;
	private GameEnvironment game;
	private User user;
	private Item item;
	/**
	 * Launch the application.
	 */
	
	public SetupScreen(GameEnvironment game1, User incomingUser) {
		game = game1;
		user = incomingUser;
		initialize();
		frmSetupscreen.setVisible(true);
	}
	
	
	public void closeWindow() {
		frmSetupscreen.dispose();
	}
	
	public void finishedWindow() {
		game.closesetupScreen(this);
	}


	/**
	 * Create the application.
	 * @param user2 
	 * @param gameEnvironment 
	 */


	/** Initialize the contents of the frame.
	 *
	 */
	
	private void initialize() {
		frmSetupscreen = new JFrame();
		frmSetupscreen.setType(Type.UTILITY);
		frmSetupscreen.getContentPane().setBackground(new Color(174, 0, 0));
		frmSetupscreen.setBackground(new Color(255, 255, 255));
		frmSetupscreen.setTitle("Quiditch Manager");
		frmSetupscreen.setBounds(100, 100, 450, 300);
		frmSetupscreen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmSetupscreen.getContentPane().setLayout(null);
		
		txtChooseTeamName = new JTextField();
		txtChooseTeamName.setText("");
		txtChooseTeamName.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String teamName = txtChooseTeamName.getText();
				try {
					game.setTeamName(teamName);
					txtChooseTeamName.setText("Welcome to Quidditch Manager " + game.getTeamName());
				} catch (IlleagalArgumentException e1) {
					// TODO Auto-generated catch block
					txtChooseTeamName.setText("Nope. Try Again and Press Enter (3-15 characters)");
				}
						
			}
		});
		txtChooseTeamName.setBounds(88, 25, 278, 19);
		txtChooseTeamName.setColumns(1);
		txtChooseTeamName.setHorizontalAlignment(SwingConstants.CENTER);
		frmSetupscreen.getContentPane().add(txtChooseTeamName);
		
		JSlider seasonLenght = new JSlider();
		seasonLenght.setValue(10);
		seasonLenght.setForeground(new Color(238, 186, 47));
		seasonLenght.setBackground(new Color(0, 0, 0));
		seasonLenght.setPaintLabels(true);
		seasonLenght.setPaintTicks(true);
		seasonLenght.setMajorTickSpacing(5);
		seasonLenght.setMinorTickSpacing(1);
		seasonLenght.setSnapToTicks(true);
		seasonLenght.setMaximum(15);
		seasonLenght.setMinimum(5);
		seasonLenght.setBounds(106, 70, 234, 40);
		frmSetupscreen.getContentPane().add(seasonLenght);
		
		btnChoosePlayers = new JButton("Choose Players");
		btnChoosePlayers.setBounds(147, 233, 162, 25);
		btnChoosePlayers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(game.getTeamName() == null) {
					JOptionPane.showMessageDialog(btnChoosePlayers, "Please Confirm A Team Name");
				} else if (game.getDificulty() == null) {
					JOptionPane.showMessageDialog(btnChoosePlayers, "Please Choose A Difficulty");
				} else {
					int wellDone = JOptionPane.showConfirmDialog(btnChoosePlayers, "Welcome " + txtChooseTeamName.getText() + "\nSeason Length: " + seasonLenght.getValue() 
					+ "\nDifficulty: " + game.getDificulty(), "Confirm Selections", JOptionPane.OK_CANCEL_OPTION);
					if(wellDone == JOptionPane.OK_OPTION) {
						game.setSeasonLength(seasonLenght.getValue());
						finishedWindow();
					}
					
				}
			}
		});
		
		
		
		frmSetupscreen.getContentPane().add(btnChoosePlayers);
		
		lblChooseSeasonLength = new JLabel("Choose Season Length (Weeks)");
		lblChooseSeasonLength.setForeground(new Color(238, 186, 47));
		lblChooseSeasonLength.setBounds(108, 54, 244, 15);
		frmSetupscreen.getContentPane().add(lblChooseSeasonLength);
		
		JLabel lblChooseDificulty = new JLabel("Choose Dificulty");
		lblChooseDificulty.setForeground(new Color(238, 186, 47));
		lblChooseDificulty.setHorizontalAlignment(SwingConstants.CENTER);
		lblChooseDificulty.setBounds(174, 124, 114, 15);
		frmSetupscreen.getContentPane().add(lblChooseDificulty);
		
		txtStartingGalleonsEasy = new JTextField();
		txtStartingGalleonsEasy.setFont(new Font("Dialog", Font.PLAIN, 10));
		txtStartingGalleonsEasy.setHorizontalAlignment(SwingConstants.LEFT);
		txtStartingGalleonsEasy.setText("Starting Galleons  - 100\n");
		txtStartingGalleonsEasy.setEditable(false);
		txtStartingGalleonsEasy.setBounds(48, 172, 134, 19);
		frmSetupscreen.getContentPane().add(txtStartingGalleonsEasy);
		txtStartingGalleonsEasy.setColumns(1);
		
		txtGalleonsPerWinEasy = new JTextField();
		txtGalleonsPerWinEasy.setText("Galleons Per Win - 25");
		txtGalleonsPerWinEasy.setHorizontalAlignment(SwingConstants.LEFT);
		txtGalleonsPerWinEasy.setFont(new Font("Dialog", Font.PLAIN, 10));
		txtGalleonsPerWinEasy.setEditable(false);
		txtGalleonsPerWinEasy.setColumns(1);
		txtGalleonsPerWinEasy.setBounds(48, 189, 134, 19);
		frmSetupscreen.getContentPane().add(txtGalleonsPerWinEasy);
		
		txtOpponentStatsEasy = new JTextField();
		txtOpponentStatsEasy.setText("Opponent Stats - (40 - 60)");
		txtOpponentStatsEasy.setEditable(false);
		txtOpponentStatsEasy.setHorizontalAlignment(SwingConstants.LEFT);
		txtOpponentStatsEasy.setFont(new Font("Dialog", Font.PLAIN, 10));
		txtOpponentStatsEasy.setColumns(1);
		txtOpponentStatsEasy.setBounds(48, 205, 134, 19);
		frmSetupscreen.getContentPane().add(txtOpponentStatsEasy);
		
		txtStartingGalleonsHard = new JTextField();
		txtStartingGalleonsHard.setEditable(false);
		txtStartingGalleonsHard.setText("Starting Galleons  - 50\n");
		txtStartingGalleonsHard.setHorizontalAlignment(SwingConstants.LEFT);
		txtStartingGalleonsHard.setFont(new Font("Dialog", Font.PLAIN, 10));
		txtStartingGalleonsHard.setColumns(1);
		txtStartingGalleonsHard.setBounds(279, 172, 134, 19);
		frmSetupscreen.getContentPane().add(txtStartingGalleonsHard);
		
		txtGalleonsPerWinHard = new JTextField();
		txtGalleonsPerWinHard.setText("Galleons Per Win - 10");
		txtGalleonsPerWinHard.setHorizontalAlignment(SwingConstants.LEFT);
		txtGalleonsPerWinHard.setFont(new Font("Dialog", Font.PLAIN, 10));
		txtGalleonsPerWinHard.setColumns(1);
		txtGalleonsPerWinHard.setEditable(false);
		txtGalleonsPerWinHard.setBounds(279, 188, 134, 19);
		frmSetupscreen.getContentPane().add(txtGalleonsPerWinHard);
		
		txtOpponentStatsHard = new JTextField();
		txtOpponentStatsHard.setText("Opponent Stats - (40+)");
		txtOpponentStatsHard.setHorizontalAlignment(SwingConstants.LEFT);
		txtOpponentStatsHard.setFont(new Font("Dialog", Font.PLAIN, 10));
		txtOpponentStatsHard.setColumns(1);
		txtOpponentStatsHard.setEditable(false);
		txtOpponentStatsHard.setBounds(279, 204, 134, 19);
		frmSetupscreen.getContentPane().add(txtOpponentStatsHard);
		
		JButton btnEasy = new JButton("Easy");
		btnEasy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				game.setDifficulty("Easy");
				game.setGalleons(100);
			}
		});
		btnEasy.setBounds(39, 119, 117, 25);
		frmSetupscreen.getContentPane().add(btnEasy);
		
		JButton btnHard = new JButton("Hard");
		btnHard.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				game.setDifficulty("Hard");
				game.setGalleons(50);
			}
		});
		btnHard.setBounds(306, 119, 117, 25);
		frmSetupscreen.getContentPane().add(btnHard);
		
		JLabel lblChooseTeamName = new JLabel("Choose Team Name (3 - 15 Characters). Press Enter");
		lblChooseTeamName.setBackground(new Color(0, 0, 0));
		lblChooseTeamName.setForeground(new Color(238, 186, 47));
		lblChooseTeamName.setBounds(36, 4, 392, 15);
		frmSetupscreen.getContentPane().add(lblChooseTeamName);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(SetupScreen.class.getResource("/images/set.jpg")));
		label.setBounds(-204, -173, 657, 482);
		frmSetupscreen.getContentPane().add(label);

	}
}
