package gui;
import java.awt.EventQueue;
import java.awt.FlowLayout;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import main.Athlete;
import main.GameEnvironment;
import main.GameMechanics;
import main.Item;
import main.User;

import javax.swing.JMenuBar;
import javax.swing.JOptionPane;

import java.awt.Choice;
import java.awt.Color;
import java.awt.Button;
import javax.swing.JProgressBar;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextPane;
import javax.swing.JLayeredPane;
import javax.swing.ImageIcon;
import javax.swing.JTextArea;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.beans.PropertyChangeEvent;
import javax.swing.DropMode;
import java.awt.Canvas;

public class MainScreen {

	private JFrame frmTeamLobby;
	private JTextField txtTeamName;
	private GameEnvironment game;
	private JTextField textField;
	private User user;
	private Athlete athelte;
	private Item item;
	private GameMechanics playGame;
	Random random = new Random();

	/**
	 * Launch the application.
	 * 
	 */


	/**
	 * Create the application.
	 *  @wbp.parser.constructor
	 * @param setUp 
	 */
	public MainScreen(GameEnvironment game1, User incomingUser) {
		game = game1;
		user = incomingUser;
		initialize();
		frmTeamLobby.setVisible(true);
	}

	public MainScreen() {
		initialize();
	}
	
	
	public void closeWindow() {
		frmTeamLobby.dispose();
	}
	
	public void launchmarketScreen() {
		game.closetogomarket(this);
		
	}
	
	public void launchclubScreen() {
		game.closetogoclub(this);
	
	}
	
	public void launchstadiumScreen() {
		game.closetogostadium(this);
	}
	
	public void luanchfinishScreen() {
		game.closetoFinish(this);
	}
	
	/**
	 * Initialize the contents of the frame.
	 * @wbp.parser.constructor
	 */
	private void initialize() {
		frmTeamLobby = new JFrame();
		frmTeamLobby.getContentPane().setBackground(new Color(174, 0, 0));
		frmTeamLobby.setTitle("Team Lobby");
		frmTeamLobby.setBounds(100, 100, 450, 300);
		frmTeamLobby.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmTeamLobby.getContentPane().setLayout(null);
		
		JButton btnMarketButton = new JButton("Market");
		btnMarketButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for(int i =0; i < 3; i++) {
					Athlete athlete = new Athlete();
					game.getShop().getAthletes().add(athlete);
					Item item = new Item(game.getDificulty());
					game.getShop().getItems().add(item);	
				}
				launchmarketScreen();
				
			}
		});
		btnMarketButton.setBounds(12, 12, 117, 25);
		frmTeamLobby.getContentPane().add(btnMarketButton);
		
		txtTeamName = new JTextField();
		txtTeamName.setText("fsafsfsa");
		txtTeamName.setHorizontalAlignment(SwingConstants.CENTER);
		txtTeamName.setEditable(false);
		txtTeamName.setBounds(162, 88, 146, 19);
		txtTeamName.setColumns(10);
		
		JProgressBar seasonProgress = new JProgressBar();
		seasonProgress.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent evt) {
				seasonProgress.setValue(game.getCurrentWeek());
				
			}
		});
		seasonProgress.setForeground(new Color(0, 0, 0));
		seasonProgress.setToolTipText("");
		seasonProgress.setStringPainted(true);
		seasonProgress.setMaximum(game.getSeasonLength());
		seasonProgress.setBackground(new Color(255, 255, 255));
		seasonProgress.setBounds(295, 244, 117, 14);
		frmTeamLobby.getContentPane().add(seasonProgress);
		
		JLabel lblSeasonProgress = new JLabel("Season Progress");
		lblSeasonProgress.setForeground(new Color(238, 186, 47));
		lblSeasonProgress.setHorizontalAlignment(SwingConstants.CENTER);
		lblSeasonProgress.setFont(new Font("Dialog", Font.BOLD, 10));
		lblSeasonProgress.setBounds(298, 228, 109, 15);
		frmTeamLobby.getContentPane().add(lblSeasonProgress);
		
		JButton btnClubButton = new JButton("Club");
		btnClubButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				launchclubScreen();
}
		});
		btnClubButton.setBounds(12, 49, 117, 25);
		frmTeamLobby.getContentPane().add(btnClubButton);
		
		JButton btnStadiumButton = new JButton("Stadium");
		btnStadiumButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				launchstadiumScreen();
			}
		});
		btnStadiumButton.setBounds(12, 85, 117, 25);
		frmTeamLobby.getContentPane().add(btnStadiumButton);
		
		JTextArea currentWeek = new JTextArea();
		currentWeek.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent evt) {
				currentWeek.setText("" + game.getCurrentWeek());
			}
		});
		currentWeek.setText("7");
		currentWeek.setBounds(323, 150, 25, 19);
		currentWeek.setEditable(false);
		frmTeamLobby.getContentPane().add(currentWeek);
		
		JTextArea remainWeeks = new JTextArea();
		remainWeeks.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent evt) {
				int weeksLeft = (game.getRemainingWeeks());
				remainWeeks.setText("" + weeksLeft);
			}
		});
		remainWeeks.setText("8");
		remainWeeks.setBounds(323, 190, 25, 19);
		remainWeeks.setEditable(false);
		frmTeamLobby.getContentPane().add(remainWeeks);
		
		JLabel lblCurrentWeek = new JLabel("Current Week");
		lblCurrentWeek.setForeground(new Color(238, 186, 47));
		lblCurrentWeek.setBounds(162, 150, 131, 15);
		frmTeamLobby.getContentPane().add(lblCurrentWeek);
		
		JLabel lblReaminingWeeks = new JLabel("Reamining Weeks");
		lblReaminingWeeks.setForeground(new Color(238, 186, 47));
		lblReaminingWeeks.setBounds(162, 190, 131, 15);
		frmTeamLobby.getContentPane().add(lblReaminingWeeks);
		
		JLabel lblCurrentBalance = new JLabel("Current Balance");
		lblCurrentBalance.setForeground(new Color(238, 186, 47));
		lblCurrentBalance.setBounds(281, 12, 131, 15);
		frmTeamLobby.getContentPane().add(lblCurrentBalance);
		
		JTextArea currentGalleons = new JTextArea();
		currentGalleons.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent evt) {
					currentGalleons.setText("" + game.getGalleons() + " ʛ");
			}
		});
		currentGalleons.setText(" 100.00 ʛ");
		currentGalleons.setBounds(281, 32, 117, 15);
		currentGalleons.setEditable(false);
		frmTeamLobby.getContentPane().add(currentGalleons);
		
		JButton btnEndGame = new JButton("End Game");
		btnEndGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				luanchfinishScreen();
			}
		});
		btnEndGame.setBounds(12, 210, 117, 25);
		frmTeamLobby.getContentPane().add(btnEndGame);
		
		JTextField teamName = new JTextField();
		teamName.setHorizontalAlignment(SwingConstants.CENTER);
		teamName.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent evt) {	
				if (game.getCurrentWeek() != game.getSeasonLength()) {
					teamName.setText(game.getTeamName());
				} else {
					teamName.setText("Thank You For Playing");

					}			
				}
		});
		teamName.setBounds(139, 120, 163, 14);
		teamName.setEditable(false);
		frmTeamLobby.getContentPane().add(teamName);
		teamName.setColumns(10);
		

		JLabel lblReaminingWeeks_1 = new JLabel("Season Wins");
		lblReaminingWeeks_1.setForeground(new Color(87, 227, 137));
		lblReaminingWeeks_1.setBounds(268, 54, 97, 15);
		frmTeamLobby.getContentPane().add(lblReaminingWeeks_1);
		
		JLabel lblReaminingWeeks_1_1_1 = new JLabel("Team Points");
		lblReaminingWeeks_1_1_1.setForeground(new Color(192, 97, 203));
		lblReaminingWeeks_1_1_1.setBounds(268, 94, 117, 15);
		frmTeamLobby.getContentPane().add(lblReaminingWeeks_1_1_1);
		
		
		JTextArea remainWeeks_1 = new JTextArea();
		remainWeeks_1.setText("" +game.getTeamWins());
		remainWeeks_1.setEditable(false);
		remainWeeks_1.setBounds(381, 54, 17, 19);
		frmTeamLobby.getContentPane().add(remainWeeks_1);
		
		JTextArea remainWeeks_1_1 = new JTextArea();
		remainWeeks_1_1.setText(""+ game.getTeamLosses());
		remainWeeks_1_1.setEditable(false);
		remainWeeks_1_1.setBounds(381, 74, 17, 19);
		frmTeamLobby.getContentPane().add(remainWeeks_1_1);
		
		JLabel lblReaminingWeeks_1_1 = new JLabel("Season Losses");
		lblReaminingWeeks_1_1.setForeground(new Color(246, 97, 81));
		lblReaminingWeeks_1_1.setBounds(268, 74, 117, 15);
		frmTeamLobby.getContentPane().add(lblReaminingWeeks_1_1);
		
		JTextArea remainWeeks_1_1_1 = new JTextArea();
		remainWeeks_1_1_1.setText("" + game.getTeamPoints());
		remainWeeks_1_1_1.setEditable(false);
		remainWeeks_1_1_1.setBounds(381, 90, 17, 19);
		frmTeamLobby.getContentPane().add(remainWeeks_1_1_1);
		
		JButton btnTrain = new JButton("Take A BYE");
		btnTrain.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
					if(game.getRemainingWeeks() != 1) {
						game.increaseWeek();
						remainWeeks.setText("" + game.getRemainingWeeks());
						currentWeek.setText("" + game.getCurrentWeek());
						seasonProgress.setValue(game.getCurrentWeek());	
						
						game.removeBought();
						//Update Shop
						game.getShop().removeItems();
						game.getShop().removePlayers();
						game.createOpponent1();
						game.createOpponent2();
						game.createOpponent3();
						
						for(int i =0; i < 3; i++) {
							Athlete athlete = new Athlete();
							game.getShop().getAthletes().add(athlete);
							Item item = new Item(game.getDificulty());
							game.getShop().getItems().add(item);	
						}
						
						
						// Random Chance of An Athlete quitting
						if(user.getAthletes().size() > 1) {
							int randomAthleteIndex = random.nextInt(user.getAthletes().size());
							Athlete randomAthlete = user.getAthletes().get(randomAthleteIndex);
							if (game.getDificulty().equalsIgnoreCase("Easy")) {
								//Easy - 0 Stam - 10% Chance
								if (randomAthlete.getStamina() <= 0) {
									int randomInt = random.nextInt(10);
									if (randomInt == 0) {
										user.removeAthlete(randomAthlete);
										JOptionPane.showMessageDialog(btnTrain, randomAthlete.getName() + " got Sent To Azkaban Prison, they are no longer in your team. They were a " + randomAthlete.getStatus());
									}
									//Easy - > 0 Stam - 5% Chance
								} else if (randomAthlete.getStamina() > 0) {
									int randomInt = random.nextInt(20);
									if(randomInt == 0) {
										user.removeAthlete(randomAthlete);
										JOptionPane.showMessageDialog(btnTrain, randomAthlete.getName() + " turned to the Dark Lord, they are no longer in your team. They were a " + randomAthlete.getStatus());
									}
								}	
							} else { 
								//Hard - 0 Stam - 20% Chance
								if(randomAthlete.getStamina() == 0) {
									int randomInt = random.nextInt(5);
									if(randomInt == 0) {
										user.removeAthlete(randomAthlete);
										JOptionPane.showMessageDialog(btnTrain, randomAthlete.getName() + " got Sent To Azkaban Prison, they are no longer in your team. They were a " + randomAthlete.getStatus());
									}
									//Hard - > 0 Stam - 14.3% Chance
								} else if (randomAthlete.getStamina() > 0) {
									int randomInt = random.nextInt(7);
									if (randomInt == 0) {
										user.removeAthlete(randomAthlete);
										JOptionPane.showMessageDialog(btnTrain, randomAthlete.getName() + " turned to the Dark Lord, they are no longer in your team. They were a " + randomAthlete.getStatus());
									}
								}								
							}
						}
						
						
						
						// Random Chance of An Athlete Joining
						Athlete randomAthlete = new Athlete();
						if (user.getAthletes().size() < 11) {
							if(user.getAthletes().size() == 8) {
								int randomInt = random.nextInt(5);
								if (randomInt ==0) {
									user.addAthlete(randomAthlete);
									randomAthlete.setStatus("Reserve");
									JOptionPane.showMessageDialog(btnTrain, randomAthlete.getName() + " joined your team on exchange from Durmstrang Institue.");
								}
							} else if (user.getAthletes().size() == 9) {
								int randomInt = random.nextInt(10);
								if (randomInt == 0) {
									user.addAthlete(randomAthlete);
									randomAthlete.setStatus("Reserve");
									JOptionPane.showMessageDialog(btnTrain, randomAthlete.getName() + " joined your team on exchange from The Uagadou School Of Magic.");
								}
							} else if (user.getAthletes().size() == 10) {
								int randomInt = random.nextInt(20);
									if (randomInt == 0) {
										user.addAthlete(randomAthlete);
										randomAthlete.setStatus("Reserve");
										JOptionPane.showMessageDialog(btnTrain, randomAthlete.getName() + " joined your team on exchange from Castelobruxo.");
								}
							}
						}
						
						// Random chance of stat increase
						List<String> statList = new ArrayList<>();
						statList.add("Offense");
						statList.add("Defense");
						statList.add("Magic");
						int randomStatIndex = random.nextInt(statList.size());
						String randomStat = statList.get(randomStatIndex);
						
						for (Athlete athlete : user.getAthletes()) {
							if (game.getTeamWins() >= (0.5 * game.getCurrentWeek())) {
								int randomInt = random.nextInt(10);
								if (randomStat.equalsIgnoreCase("Offense")) {
									if(randomInt == 0) {
										athlete.setOffense(athlete.getOffense() + 10);
										JOptionPane.showMessageDialog(btnTrain, athlete.getName() + " drank a Wideye Potion which increased thier offence by 10.");
									}
								} else if (randomStat.equalsIgnoreCase("Defense")) {
									if(randomInt == 0) {
										athlete.setOffense(athlete.getDefense() + 10);
										JOptionPane.showMessageDialog(btnTrain, athlete.getName() + " drank a Endurus Potion which increased thier Defence by 10.");
									}
								} else if (randomStat.equalsIgnoreCase("Magic")) {
									if(randomInt == 0) {
										athlete.setOffense(athlete.getOffense() + 10);
										JOptionPane.showMessageDialog(btnTrain, athlete.getName() + " drank a Exstimulo Potion which increased thier Magic by 10.");
									}
								}
							} else {
								int randomInt = random.nextInt(5);
								if (randomStat.equalsIgnoreCase("Offense")) {
									if(randomInt == 0) {
										athlete.setOffense(athlete.getOffense() + 10);
										JOptionPane.showMessageDialog(btnTrain, athlete.getName() + " drank a Wideye Potion which increased thier offence by 10.");
									}
								} else if (randomStat.equalsIgnoreCase("Defense")) {
									if(randomInt == 0) {
										athlete.setOffense(athlete.getDefense() + 10);
										JOptionPane.showMessageDialog(btnTrain, athlete.getName() + " drank a Endurus Potion which increased thier Defence by 10.");
									}
								} else if (randomStat.equalsIgnoreCase("Magic")) {
									if(randomInt == 0) {
										athlete.setOffense(athlete.getOffense() + 10);
										JOptionPane.showMessageDialog(btnTrain, athlete.getName() + " drank a Exstimulo Potion which increased thier Magic by 10.");
									}
								}
								
							}
						
						}
						
					
						
						
						
						for(Athlete athlete : user.getAthletes()) {
							athlete.setStam(100);
						}
						
						int choice = JOptionPane.showConfirmDialog(null, "Do you want to train a player?", "Train Player", JOptionPane.YES_NO_OPTION);
				        if (choice == JOptionPane.YES_OPTION) {
				            // Show a dialog to select an athlete to train
				            JDialog trainDialog = new JDialog();
				            // Set dialog properties such as title, size, layout, etc.
				            trainDialog.setTitle("Select Athlete to Train");
				            trainDialog.setSize(400, 300);
				            trainDialog.getContentPane().setLayout(new FlowLayout());
				            
				            JLabel lblSelectAthlete = new JLabel("Select Athlete:");
				            trainDialog.getContentPane().add(lblSelectAthlete);
				            
				            // Add components to the dialog, such as a JComboBox to select athletes
				            JComboBox athleteComboBox = new JComboBox<>();
				            for(Athlete athlete : user.getAthletes()) {
				            	athleteComboBox.addItem(athlete);
				            }
				            trainDialog.getContentPane().add(athleteComboBox);
				            
				            JComboBox<String> statBox = new JComboBox<>();
				            statBox.addItem("Offense + 10");
				            statBox.addItem("Defense + 10");
				            statBox.addItem("Magic + 5");
				            
				           
				            trainDialog.getContentPane().add(statBox);
				            
				            JButton btnSelect = new JButton("Select");
				            btnSelect.addActionListener(new ActionListener() {
				                public void actionPerformed(ActionEvent e) {
				                	Athlete selectedAthlete = ((Athlete) athleteComboBox.getSelectedItem());
				                	String selectedStat = (String) statBox.getSelectedItem();
				                	if (selectedStat == null ) {
				                		JOptionPane.showMessageDialog(btnTrain, "Select an Stat to train.");
				                	} else if (selectedAthlete == null) {
				                		JOptionPane.showMessageDialog(btnTrain, "Select a Athlete to train");
				                	} else {
				                    // Perform the necessary actions when the athlete is selected
				                    if(selectedStat.equalsIgnoreCase("Offense + 10")) {
				                    	selectedAthlete.setOffense(selectedAthlete.getOffense() + 10);
				                    } else if (selectedStat.equalsIgnoreCase("Defense + 10")) {
				                    	selectedAthlete.setDeffense(selectedAthlete.getDefense() + 10);
				                    } else if (selectedStat.equalsIgnoreCase("Magic + 5")){
				                    	selectedAthlete.setMagic(selectedAthlete.getMagic() + 5);
				                    }
				                	}
				                    
				                    // Perform training actions for the selected athlete
				                    
				                    trainDialog.dispose(); // Close the dialog
				                }
				            });
				            trainDialog.getContentPane().add(btnSelect);
				            
				            trainDialog.setVisible(true); // Show the dialog
				        }
						
					} else {
						game.increaseWeek();
						remainWeeks.setText("" + game.getRemainingWeeks());
						currentWeek.setText("" + game.getCurrentWeek());
						seasonProgress.setValue(game.getCurrentWeek());
						JOptionPane.showMessageDialog(btnEndGame, "Congradulations, The Season Is Over. Click End Game");
						btnStadiumButton.setEnabled(false);
						btnClubButton.setEnabled(false);
						btnMarketButton.setEnabled(false);
						btnTrain.setEnabled(false);
					}
				}  
		    
		});
		btnTrain.setBounds(12, 167, 117, 25);
		frmTeamLobby.getContentPane().add(btnTrain);

		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(MainScreen.class.getResource("/images/set.jpg")));
		label.setBounds(-106, -56, 657, 482);
		frmTeamLobby.getContentPane().add(label);
		

	}
}


