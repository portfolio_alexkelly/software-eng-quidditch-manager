package gui;

import java.awt.EventQueue;



import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Random;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

import main.Athlete;
import main.GameEnvironment;
import main.Item;
import main.User;

import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;

public class MarketScreen {

	private JFrame frmMarket;
	private GameEnvironment game;
	private MainScreen main;
	private Item item;
	private User user;
	private Athlete athlete;
	private int price;
	private JButton button;
	private int priceInt;
	private Random random = new Random();
	private boolean isTypeAthlete;


	

	/**
	 * Launch the application.
	 */

	/**
	 * @wbp.parser.constructor
	 * Create the application.
	 */
	public MarketScreen(GameEnvironment game1, User incomingUser) {
		game = game1;
		user = incomingUser;
		initialize();
		frmMarket.setVisible(true);
	}
	
	public void closeWindow() {
		frmMarket.dispose();
	}
	
	public void backtomain() {
		game.closemarketScreen(this);
	}
	
	public MarketScreen() {
		initialize();
	}
	public void toSellpage() {
		game.closToGoSellScreen(this);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMarket = new JFrame();
		frmMarket.getContentPane().setBackground(new Color(174, 0, 0));
		frmMarket.setTitle("Market");
		frmMarket.setBounds(100, 100, 450, 300);
		frmMarket.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMarket.getContentPane().setLayout(null);
		
		JTextArea productInfoText = new JTextArea();
		productInfoText.setFont(new Font("Dialog", Font.PLAIN, 12));
		productInfoText.setBounds(176, 64, 252, 137);
		frmMarket.getContentPane().add(productInfoText);
		
		JLabel lblNewLabel = new JLabel("Items for Sale\n");
		lblNewLabel.setForeground(new Color(255, 255, 255));
		lblNewLabel.setBounds(17, 40, 107, 15);
		frmMarket.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Players for Sale");
		lblNewLabel_1.setForeground(new Color(255, 255, 255));
		lblNewLabel_1.setBounds(17, 160, 121, 15);
		frmMarket.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Current Balance");
		lblNewLabel_2.setForeground(new Color(238, 186, 47));
		lblNewLabel_2.setBounds(304, 12, 124, 15);
		frmMarket.getContentPane().add(lblNewLabel_2);
		
		JTextArea CurrencyText = new JTextArea();
		CurrencyText.setText("ʛ " + game.getGalleons());
		CurrencyText.setBounds(304, 31, 114, 15);
		frmMarket.getContentPane().add(CurrencyText);
		
		JButton btnNewButton = new JButton("To Sell Page");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				toSellpage();
			}
		});
		
		
		
		
		btnNewButton.setBounds(309, 235, 124, 25);
		frmMarket.getContentPane().add(btnNewButton);
		
		JButton ItemButtonOne = new JButton(game.getShop().getItems().get(0).getName());
		ItemButtonOne.setName("item1");
		if(game.getBought().contains(ItemButtonOne.getName())) {
			ItemButtonOne.setEnabled(false);
		}
		ItemButtonOne.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				productInfoText.setText("Currently Selected Item \n"+ game.getShop().getItems().get(0).getName() + "\n" + game.getShop().getItems().get(0).getDescription()
				+ "\nIt Costs: " + game.getShop().getItems().get(0).getPrice() + " Galleons");
				price = game.getShop().getItems().get(0).getPrice();
				item = game.getShop().getItems().get(0);
				isTypeAthlete = false;
				button = ItemButtonOne;
				
					}
			}
		);
		
		ItemButtonOne.setBounds(12, 67, 152, 25);
		frmMarket.getContentPane().add(ItemButtonOne);
		
		
		JButton btnNewButton_1_4 = new JButton(game.getShop().getAthletes().get(0).getName());
		btnNewButton_1_4.setName("athlete1");
		if(game.getBought().contains(btnNewButton_1_4.getName())) {
			btnNewButton_1_4.setEnabled(false);
		}
		btnNewButton_1_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				priceInt = random.nextInt(10,40);
				productInfoText.setText("Currently Selected Athlete \n"+ game.getShop().getAthletes().get(0).getName() + "\n" + game.getShop().getAthletes().get(0).getDescription()
				+ "\nIt Costs: " + game.getShop().getAthletes().get(0).getPrice() + " Galleons");
				athlete = game.getShop().getAthletes().get(0);
				price = game.getShop().getAthletes().get(0).getPrice();
				isTypeAthlete = true;
				button = btnNewButton_1_4;
				}
			}
		);
		
		btnNewButton_1_4.setBounds(12, 176, 152, 25);
		frmMarket.getContentPane().add(btnNewButton_1_4);

		
		JButton ItemButtonOne_1 =  new JButton(game.getShop().getItems().get(1).getName());
		ItemButtonOne_1.setName("item2");
		if(game.getBought().contains(ItemButtonOne_1.getName())) {
			ItemButtonOne_1.setEnabled(false);
		}
		
		ItemButtonOne_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				productInfoText.setText("Currently Selected Item \n"+ game.getShop().getItems().get(1).getName() + "\n" + game.getShop().getItems().get(1).getDescription()
						+ "\nIt Costs: " + game.getShop().getItems().get(1).getPrice() + " Galleons");
						price = game.getShop().getItems().get(1).getPrice();
						item = game.getShop().getItems().get(1);
						isTypeAthlete = false;
						button = ItemButtonOne_1;
			}
		});
			
		
		ItemButtonOne_1.setBounds(12, 96, 152, 25);
		frmMarket.getContentPane().add(ItemButtonOne_1);
		
		JButton ItemButtonOne_2 =  new JButton(game.getShop().getItems().get(2).getName());
		ItemButtonOne_2.setName("item3");
		if(game.getBought().contains(ItemButtonOne_2.getName())) {
			ItemButtonOne_2.setEnabled(false);
		}
		ItemButtonOne_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				productInfoText.setText("Currently Selected Item \n"+ game.getShop().getItems().get(2).getName() + "\n" + game.getShop().getItems().get(2).getDescription()
						+ "\nIt Costs: " + game.getShop().getItems().get(2).getPrice() + " Galleons");
						price = game.getShop().getItems().get(2).getPrice();
						item = game.getShop().getItems().get(2);
						isTypeAthlete = false;
						button = ItemButtonOne_2;
			}
		});
		ItemButtonOne_2.setBounds(12, 127, 152, 25);
		frmMarket.getContentPane().add(ItemButtonOne_2);
		
		JButton btnNewButton_1_4_1 = new JButton(game.getShop().getAthletes().get(1).getName());
		btnNewButton_1_4_1.setName("player2");
		if(game.getBought().contains(btnNewButton_1_4_1.getName())) {
			btnNewButton_1_4_1.setEnabled(false);
		}
		btnNewButton_1_4_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				priceInt = random.nextInt(10,40);
				productInfoText.setText("Currently Selected Athlete \n"+ game.getShop().getAthletes().get(1).getName() + "\n" + game.getShop().getAthletes().get(1).getDescription()
				+ "\nIt Costs: " + game.getShop().getAthletes().get(1).getPrice() + " Galleons");
				athlete = game.getShop().getAthletes().get(1);
				price = game.getShop().getAthletes().get(1).getPrice();
				isTypeAthlete = true;
				button = btnNewButton_1_4_1;
			}
		});
		btnNewButton_1_4_1.setBounds(12, 206, 152, 25);
		frmMarket.getContentPane().add(btnNewButton_1_4_1);
		
		JButton btnNewButton_1_4_2 = new JButton(game.getShop().getAthletes().get(2).getName());
		btnNewButton_1_4_2.setName("player3");
		if(game.getBought().contains(btnNewButton_1_4_2.getName())) {
			btnNewButton_1_4_2.setEnabled(false);
		}
		btnNewButton_1_4_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				priceInt = random.nextInt(10,40);
				productInfoText.setText("Currently Selected Athlete \n"+ game.getShop().getAthletes().get(2).getName() + "\n" + game.getShop().getAthletes().get(2).getDescription()
				+ "\nIt Costs: " + game.getShop().getAthletes().get(2).getPrice() + " Galleons");
				athlete = game.getShop().getAthletes().get(2);
				price = game.getShop().getAthletes().get(2).getPrice();
				isTypeAthlete = true;
				button = btnNewButton_1_4_2;
		
			}
		});
		btnNewButton_1_4_2.setBounds(12, 235, 152, 25);
		frmMarket.getContentPane().add(btnNewButton_1_4_2);
		
		JLabel lblProductInfo = new JLabel("Product Info");
		lblProductInfo.setForeground(Color.WHITE);
		lblProductInfo.setBounds(176, 37, 107, 15);
		frmMarket.getContentPane().add(lblProductInfo);
		
		JButton btnToBuy = new JButton("Buy Product");
		btnToBuy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(athlete == null && item == null) {
					JOptionPane.showMessageDialog(btnToBuy, "Please select a Item or Athlete to puchase.");
				} else {
					if (game.getGalleons() < price) {
						JOptionPane.showMessageDialog(btnToBuy, "Insuffcient Galleons to purhcase.");
					} else {
						if (isTypeAthlete == true && user.getAthletes().size() == 10) {
							JOptionPane.showMessageDialog(btnToBuy, "Sorry, you aready own the max of 10 athletes.");
						} else {
							if(isTypeAthlete == true) {
								athlete.buy(game);
								athlete.setStatus("Reserve");
								JOptionPane.showMessageDialog(btnToBuy, "Congradulations you pruchased " + athlete + "\n it cost you " + price + " Galleons. The athlete's Status is set as a reserve. To change this please visit The Club.");
							} else {
								item.buy(game);
								JOptionPane.showMessageDialog(btnToBuy, "Congradulations you pruchased " + item + "\n it cost you " + price + " Galleons");
							}
							game.getBought().add(button.getName());
							button.setEnabled(false);
							CurrencyText.setText("ʛ " + game.getGalleons());
							
							
						}
					}
				}
				
					
					
					  
				}
		});
		btnToBuy.setBounds(176, 206, 133, 25);
		frmMarket.getContentPane().add(btnToBuy);
		
		JButton btnBackToMenu = new JButton("Back to Menu");
		btnBackToMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				backtomain();
			
				
			}
		});
		btnBackToMenu.setBounds(12, 2, 133, 25);
		frmMarket.getContentPane().add(btnBackToMenu);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(MarketScreen.class.getResource("/images/marketScreen.jpg")));
		label.setBounds(-142, -120, 657, 464);
		frmMarket.getContentPane().add(label);
		
	}
}