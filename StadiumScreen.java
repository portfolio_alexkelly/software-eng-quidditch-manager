package gui;


import java.awt.EventQueue;



import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;

import main.Athlete;
import main.GameEnvironment;
import main.User;

import javax.swing.JComboBox;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;

public class StadiumScreen {

	private JFrame frmStadium;
	private GameEnvironment game;
	private User user;
	private String selectedOption;


	/**
	 * Launch the application.
	 *  @wbp.parser.constructor
	 */
	
	
	
	public StadiumScreen(GameEnvironment game1, User incomingUser) {
		game = game1;
		user = incomingUser;
		initialize();
		frmStadium.setVisible(true);
	}
	
	public void closeWindow() {
		frmStadium.dispose();
	}
	
	public void backtomain() {
		game.closestadiumScreentoMain(this);
	}
	
	public void closetogogame() {
		game.closetogogame(this);
		
	}
	
	public StadiumScreen() {
		initialize();
	}

	/**
	 * Create the application.
	 


	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmStadium = new JFrame();
		frmStadium.getContentPane().setBackground(new Color(174, 0, 0));
		frmStadium.setTitle("Stadium");
		frmStadium.setBounds(100, 100, 450, 300);
		frmStadium.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmStadium.getContentPane().setLayout(null);
		
		JButton backToMenuButton = new JButton("Back to Menu");
		backToMenuButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				backtomain();
			}
		});
		backToMenuButton.setFont(new Font("Dialog", Font.BOLD, 9));
		backToMenuButton.setBounds(12, 12, 99, 25);
		frmStadium.getContentPane().add(backToMenuButton);
		
		JLabel team1Label = new JLabel("Team 1");
		team1Label.setForeground(new Color(238, 186, 47));
		team1Label.setBounds(41, 49, 70, 15);
		frmStadium.getContentPane().add(team1Label);
		
		JLabel lblTeam2 = new JLabel("Team 2");
		lblTeam2.setForeground(new Color(238, 186, 47));
		lblTeam2.setBounds(195, 49, 70, 15);
		frmStadium.getContentPane().add(lblTeam2);
		
		JLabel lblTeam3 = new JLabel("Team 3");
		lblTeam3.setForeground(new Color(238, 186, 47));
		lblTeam3.setBounds(314, 49, 70, 15);
		frmStadium.getContentPane().add(lblTeam3);
		
		JTextArea txtrTeam1Info = new JTextArea();
		txtrTeam1Info.setText("Offense: " + game.getEnemyTeamOffense(game.getOpponent1()) + "\nDefense: " + game.getEnemyTeamDefense(game.getOpponent1()) + " \nStamina: " + game.getEnemyTeamStamina(game.getOpponent1()) + "\nMagic:" + game.getEnemyTeamMagic(game.getOpponent1()));
		txtrTeam1Info.setBounds(23, 76, 99, 90);
		frmStadium.getContentPane().add(txtrTeam1Info);
		txtrTeam1Info.setEditable(false);
		
		JTextArea txtrTeam2Info = new JTextArea();
		txtrTeam2Info.setText("Offense: " + game.getEnemyTeamOffense(game.getOpponent2()) + "\nDefense: " + game.getEnemyTeamDefense(game.getOpponent2()) + " \nStamina: " + game.getEnemyTeamStamina(game.getOpponent2()) + "\nMagic:" + game.getEnemyTeamMagic(game.getOpponent2()));
		txtrTeam2Info.setBounds(180, 76, 99, 90);
		frmStadium.getContentPane().add(txtrTeam2Info);
		txtrTeam2Info.setEditable(false);

		
		JTextArea txtrTeam3Info = new JTextArea();
		txtrTeam3Info.setText("Offense: " + game.getEnemyTeamOffense(game.getOpponent3()) + "\nDefense: " + game.getEnemyTeamDefense(game.getOpponent3()) + " \nStamina: " + game.getEnemyTeamStamina(game.getOpponent3()) + "\nMagic:" + game.getEnemyTeamMagic(game.getOpponent3()));
		txtrTeam3Info.setBounds(308, 76, 99, 90);
		frmStadium.getContentPane().add(txtrTeam3Info);
		txtrTeam3Info.setEditable(false);

		
		JLabel lblTeamSelect = new JLabel("Team Select");
		lblTeamSelect.setForeground(new Color(238, 186, 47));
		lblTeamSelect.setBounds(180, 178, 99, 15);
		frmStadium.getContentPane().add(lblTeamSelect);
		
		JComboBox teamToPlay = new JComboBox();
		teamToPlay.setModel(new DefaultComboBoxModel(new String[] {game.getOpponent1Name(), game.getOpponent2Name(), game.getOpponent3Name()}));
		teamToPlay.setBounds(172, 195, 119, 25);
		frmStadium.getContentPane().add(teamToPlay);
		teamToPlay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String selectedOpponentName = (String) teamToPlay.getSelectedItem();
				game.setSelectedOpponentName(selectedOpponentName);
				selectedOption = (String) teamToPlay.getSelectedItem();
			}
		});
		
		JButton btnPlay = new JButton("Play");
		btnPlay.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    	int specialCount =0;
		        if (selectedOption == null) {
		            JOptionPane.showMessageDialog(btnPlay, "Please Select A Team To Play.");
		        } else {
		            boolean allAthletesHaveStamina = true;
		            int count = 0;
		            for (Athlete athlete : user.getAthletes()) {
		                if (athlete.getStatus().equalsIgnoreCase("Starter")) {
		                	specialCount ++;
		                    if (athlete.getStamina() <= 0) {
		                        count++;
		                        allAthletesHaveStamina = false;
		                        JOptionPane.showMessageDialog(btnPlay, "At least one of your Starters has no Stamina, please Regenerate");
		                        break; // Exit the loop if any athlete has no stamina
		                    }
		                }
		            }

		            if (allAthletesHaveStamina) {
		                int confirmChoice = JOptionPane.showConfirmDialog(btnPlay, "Are you sure you want to play with " + specialCount + " Players", "Check", JOptionPane.OK_CANCEL_OPTION);
		                if (confirmChoice == JOptionPane.YES_OPTION) {
		                    game.increaseWeek();
		                    game.getCurrentWeek();
		                    closetogogame();
		                }
		            }
		        }
		    }
		});


		btnPlay.setBounds(174, 226, 117, 25);
		frmStadium.getContentPane().add(btnPlay);
	

		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(StadiumScreen.class.getResource("/images/stadium2.jpg")));
		label.setBounds(-334, -407, 1122, 985);

		frmStadium.getContentPane().add(label);
		
		}
	}






