package main;

/**
 * An interface that either allows for n item to be bought or sold.
 *
 *
 */
public interface MarketMechanics {

	
	/**
	 * Buys the currently selected item/player.
	 * 
	 * @param environment The current game environment 
	 */
	void buy(GameEnvironment environment);
	
	/**
	 * Sells the currently selected item/player.
	 * 
	 * @param environment The current game environment
	 */
	void sell(GameEnvironment environment);
		
}
