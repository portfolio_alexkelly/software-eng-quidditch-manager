package main;

import java.util.ArrayList;

import java.util.List;
import java.util.Random;

public class Item implements MarketMechanics, UseMechanics{
	
	private String name;
	
	private User user;
	
	private GameEnvironment game;
	
	private String description;
	
	private String type;
	
	private int price;
	
	private String smallDescription;
	
	private int statIncrease;
	
	private List<String> items = new ArrayList<>();
	
	private Random random = new Random();
	
	public Item(String difficulty) {
		items.add("Broomstick");
		items.add("New Uniform");
		items.add("Magic Potion");
		items.add("Nimbus 2000");
		items.add("Felix Felicis");
		items.add("Horntail Suit");
		createItem(difficulty);
	}
	
	public Item(String tempName, int tempPrice, int tempStatIncrease) {
		name = tempName;
		price = tempPrice;
		statIncrease = tempStatIncrease;
		description = "This is a " + name + "\nIt increases your ahtletes stat by " + statIncrease + "\n" + smallDescription;
	}
	
	public String getName() {
		return name;
	}

	public String getDescription() {  
		return description;
	}
	
	public String getSmallDescription() {
		return smallDescription;

	}
	public int getPrice() {
		return price;
	}
	
	public String getType() {
		return type;
	}
	
	public int getStatIncrease() {
		return statIncrease;
	}
	
	public String toString() {
		return name;
	}
	public List<String> getItemNames() {
		return items;
	}

	@Override
	public void buy(GameEnvironment environment) {
		// TODO Auto-generated method stub
		environment.setGalleons(environment.getGalleons()- getPrice());
		environment.getUser().addItem(this);
		
	}

	@Override
	public void sell(GameEnvironment environment) {
		// TODO Auto-generated method stub
		int salePrice = (int) (getPrice() * 0.75);
		environment.setGalleons(environment.getGalleons() + salePrice);
		environment.getUser().removeItem(this);
	}
	
	public void createItem(String Difficulty) {
		int randomName = random.nextInt(6);
		this.name = items.get(randomName);
		int randomStatIncrease;
		if (name.equalsIgnoreCase("Nimbus 2000")){
			randomStatIncrease = random.nextInt(20,35);
			int price1 = random.nextInt(40,50);
			this.price = price1;
			this.statIncrease = randomStatIncrease;	
			smallDescription = " On Offense\nAnd Defense Stats.";
			
		} else if (name.equalsIgnoreCase("Horntail Suit")) {
			randomStatIncrease = random.nextInt(20,35);
			int price1 = random.nextInt(40,50);
			this.price = price1;
			this.statIncrease = randomStatIncrease;
			smallDescription = " On All Stats.";
			
		} else if (name.equalsIgnoreCase("Felix Felicis")) {
			randomStatIncrease = random.nextInt(20,35);
			int price1 = random.nextInt(40,50);
			this.price = price1;
			this.statIncrease = randomStatIncrease;	
			smallDescription = " On Magic Stat. \n(It Also Boosts Your Stamina For One Game)";
			
		} else if (name.equalsIgnoreCase("broomstick")) {
			randomStatIncrease = random.nextInt(10,20);
			int price1 = random.nextInt(10,20);
			this.price = price1;
			this.statIncrease = randomStatIncrease;
			smallDescription = " On Offense\nAnd Defense Stats.";
			
		} else if(name.equalsIgnoreCase("New uniform")) {
			smallDescription = " On All Stats.";
			randomStatIncrease = random.nextInt(10,20);
			int price1 = random.nextInt(10,20);
			this.price = price1;
			this.statIncrease = randomStatIncrease;
			
		} else if (name.equalsIgnoreCase("Magic Potion")) {
			smallDescription = " On Magic Stat. \n(It Also Boosts Your Stamina For One Week)";
			randomStatIncrease = random.nextInt(10,20);
			int price1 = random.nextInt(10,20);
			this.price = price1;
			this.statIncrease = randomStatIncrease;
		}
		this.description = "This is a " + name + "\nIt gives your athlete " + statIncrease + smallDescription;	
	}
	@Override
	public void useItem(Athlete athlete, User user) {
		if (this.name.equalsIgnoreCase("Broomstick") || this.name.equalsIgnoreCase("Nimbus 2000")) {
			athlete.setOffense(this.statIncrease + athlete.getOffense());
			athlete.setDeffense(this.statIncrease + athlete.getDefense());
		} 
		
		if(this.name.equalsIgnoreCase("New Uniform") || this.name.equalsIgnoreCase("Horntail Suit")) {
			athlete.setOffense(this.statIncrease + athlete.getOffense());
			athlete.setDeffense(this.statIncrease + athlete.getDefense());
			athlete.setMagic(this.statIncrease + athlete.getMagic());
			athlete.setOverall(this.statIncrease + athlete.getOverall());
		}
		if (this.name.equalsIgnoreCase("Magic Potion") || this.name.equalsIgnoreCase("Felix Felicis")) {
			athlete.setMagic(this.statIncrease + athlete.getMagic());
			athlete.setStam(statIncrease + athlete.getStamina());
		}
		
		user.removeItem(this);
	}
	
	
	

}
