package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class User {
	
	private String usersName;
	
	private Athlete athlete;
	
	private String teamName;
	
	
	private int galleons;
	
	private GameEnvironment game;
	
	/**
	 * A Random number used for creating random events.
	 */
	private Random random = new Random();
	/**
	 * The users points.
	 */
	private int userPoints = 0;
	/**
	 * The users list of athletes.
	 */
	private List<Athlete> usersAthletes = new ArrayList<>();
	/**
	 * The players list of items.
	 */
	private List<Item> items = new ArrayList<>();
	
	private List<String> teamNames = new ArrayList<String>();
	
	private static List<String> atheleteNames = new ArrayList<String>();
	
	
	private List<Athlete>  Team = new ArrayList<Athlete>();
	
	private List<Athlete> enemyTeam = new ArrayList<Athlete>();
	
	public User() {
		teamNames.add("Gryffindor");
        teamNames.add("Slytherin");
        teamNames.add("Hufflepuff");
        teamNames.add("Ravenclaw");
        teamNames.add("Puddlemere United");
        teamNames.add("Chudley Cannons");
        teamNames.add("Kenmare Kestrels");
        teamNames.add("Wigtown Wanderers");
        teamNames.add("Montrose Magpies");
        teamNames.add("Ballycastle Bats");
        teamNames.add("Appleby Arrows");
        teamNames.add("Holyhead Harpies");
        teamNames.add("Caerphilly Catapults");
        teamNames.add("Falmouth Falcons");
        teamNames.add("Portree Priders");
	}
	
	public String getUserName() {
		return usersName;
	}
	/**
	 * Returns the players points
	 * 
	 * @return the players points
	 */
	public  int getUsersPoints() {
		return userPoints;
	}
	/**
	 * Returns a list of the players monsters.
	 * 
	 * @return a list of the players monsters
	 */
	public List<Athlete> getAthletes() {
		return usersAthletes;
	}
	/**
	 * Returns a list of the players items.
	 * 
	 * @return a list of the players items
	 */
	public List<Item> getItems() {
		return items;
	}
	
	public void setGalleons(int gal) {
		this.galleons = gal;
	}
	
	/**
	   * Getter method to get the current balance of galleons.
	   */
	
	public int getGalleons() {
		return galleons;
	}
	
	public void setUserPoints(int points) {
		userPoints = points;
	}
	
	public void addItem(Item item) {
		items.add(item);
	}
	/**
	 * Removes an item from the list of players items.
	 * 
	 * @param item An item
	 */
	public void removeItem(Item item) {
		items.remove(item);
	}
	
	public void addAthlete(Athlete athlete) {
		usersAthletes.add(athlete);
	}
	
	
	public void removeAthlete(Athlete athlete) {
		usersAthletes.remove(athlete);
	}
	
	public int getTeamSize() {
		return usersAthletes.size();
	
	}
	
	private List<String> getTeamNames() {
		return teamNames;
	}
	
	public void setTeamName(String newTeamName)
	{
		this.teamName = newTeamName; 
	}
	
	/**
	 * Creates random Oppostion Team with four players
	 */
	public void makeTeam() {
		this.Team = new ArrayList<Athlete>();
		int randomTeamName = random.nextInt(getTeamNames().size());
		this.setTeamName(getTeamNames().get(randomTeamName));

		for(int i=0; i <=3; i++) {
			athlete.createAhlete();
			Team.add(athlete);
			}
	}
	
	public void gameResultAllocation() {

		if(game.getDificulty().equalsIgnoreCase("easy")) {
			
		} else {
			
		}
		
	}
	
	public void setAthletesNames()
	{
		this.teamNames = new ArrayList<String>();
		for (Athlete eachAthlete: Team)
		{
			teamNames.add(eachAthlete.getName());
		}
	}
	
	public List<String> getAthleteNames() {
		setAthletesNames();
		return teamNames;
	}
	

	public List<Athlete> createEnemyTeam()
	{
		this.enemyTeam = new ArrayList<Athlete>();
		int randomTeamName = random.nextInt(getTeamNames().size());
		this.setTeamName(getTeamNames().get(randomTeamName));

		for(int i=0; i <=3; i++) {
			athlete.createEnemyAthlete();
			enemyTeam.add(athlete);
			}
		return enemyTeam;
	}
	
	public int getEnemyTeamOffense(List<Athlete> enemyTeam)
	{
		int teamOffense = 0;
		for (Athlete enemy: enemyTeam)
		{
			teamOffense += enemy.getOffense();
		}
		teamOffense = teamOffense / enemyTeam.size();
		return teamOffense;
	}
	
	public int getEnemyTeamDefense(List<Athlete> enemyTeam)
	{
		int teamDefense = 0;
		for (Athlete enemy: enemyTeam)
		{
			teamDefense += enemy.getDefense();
		}
		teamDefense = teamDefense / enemyTeam.size();
		return teamDefense;
	}
	
	public int getEnemyTeamMagic(List<Athlete> enemyTeam)
	{
		int teamMagic = 0;
		for (Athlete enemy: enemyTeam)
		{
			teamMagic += enemy.getMagic();
		}
		teamMagic = teamMagic / enemyTeam.size();
		return teamMagic;
	}
	
	public int getEnemyTeamStamina(List<Athlete> enemyTeam)
	{
		int teamStamina = 0;
		for (Athlete enemy: enemyTeam)
		{
			teamStamina += enemy.getStamina();
		}
		teamStamina = teamStamina / enemyTeam.size();
		return teamStamina;
	}
	

}


	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	


