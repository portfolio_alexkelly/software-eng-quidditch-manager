package main;
import java.util.List;

import java.util.SplittableRandom;

/**
	   * All of the mechanics inside of the game.
	   */

public class GameMechanics 
{
	
	private User user;
	/**
	   * Score of the players team in the game.
	   */
	private int myTeamScore;
	
	private GameEnvironment game;
	private Athlete athlete;
	/**
	   * Score of the opponent's team in this game.
	   */
	
	private int opposingTeamScore;
	
	
	public GameMechanics(int myTeamScore, int opposingTeamScore)
	{
		this.myTeamScore = myTeamScore;
		this.opposingTeamScore = opposingTeamScore;
	}
	
	/**
	   * Getter method for the player's Team Score.
	   */
	
	public int getMyTeamScore()
	{
		return myTeamScore;
	}
	
	/**
	   * Getter method for the opponent's Team Score.
	   */
	
	public int getOpposingTeamScore()
	{
		return opposingTeamScore;
	}
	
	public void incOpposingTeamScore(int val) {
		opposingTeamScore += val;
	}
	
	public void incMyTeamScore(int val) {
		myTeamScore += val;
	}
	
	/**
	   * Instance of player's attacker vs opponent's defender.
	   */
	
	public void headToHeadAttack (Athlete teamAthlete, Athlete opposingAthlete)
	{
		if (teamAthlete.getOffense() > opposingAthlete.getDefense())
		{
			myTeamScore += 1;
			teamAthlete.useStamina(10);
			opposingAthlete.useStamina(20);
		}
		else if (teamAthlete.getOffense() < opposingAthlete.getDefense())
		{
			opposingTeamScore += 1;
			teamAthlete.useStamina(20);
			opposingAthlete.useStamina(10);
		}
		else if (teamAthlete.getOffense() == opposingAthlete.getDefense())
		{
			if (teamAthlete.getMagic() >= opposingAthlete.getMagic())
			{
				myTeamScore += 1;
				teamAthlete.useStamina(10);
				opposingAthlete.useStamina(20);
			}
			else
			{
				opposingTeamScore += 1;
				teamAthlete.useStamina(20);
				opposingAthlete.useStamina(10);
			}
		}
	}
	
	/**
	   * Instance of player's defender vs opponent's attacker.
	   */
	
	public void headToHeadDefense (Athlete teamAthlete, Athlete opposingAthlete)
	{
		if (teamAthlete.getDefense() > opposingAthlete.getOffense())
		{
			myTeamScore += 1;
			teamAthlete.useStamina(10);
			opposingAthlete.useStamina(20);
		}
		else if (teamAthlete.getDefense() < opposingAthlete.getOffense())
		{
			opposingTeamScore += 1;
			teamAthlete.useStamina(20);
			opposingAthlete.useStamina(10);
		}
		else if (teamAthlete.getDefense() == opposingAthlete.getOffense())
		{
			if (teamAthlete.getMagic() >= opposingAthlete.getMagic())
			{
				myTeamScore += 1;
				teamAthlete.useStamina(10);
				opposingAthlete.useStamina(20);
			}
			else
			{
				opposingTeamScore += 1;
				teamAthlete.useStamina(20);
				opposingAthlete.useStamina(10);
			}
		}
	}
	
	/**
	   * The head to head match when the golden snitch appears.
	   */
	
	public void snitchHeadToHead (Athlete teamAthlete, Athlete opposingAthlete)
	{
		if (teamAthlete.getMagic() > opposingAthlete.getMagic())
		{
			myTeamScore += 100;
			teamAthlete.useStamina(25);
			opposingAthlete.useStamina(75);
		}
		else if (teamAthlete.getMagic() < opposingAthlete.getMagic())
		{
			opposingTeamScore += 100;
			teamAthlete.useStamina(75);
			opposingAthlete.useStamina(25);
		}
		else if (teamAthlete.getMagic() == opposingAthlete.getMagic())
		{
			if (teamAthlete.getOverall() >= opposingAthlete.getOverall())
			{
				myTeamScore += 100;
				teamAthlete.useStamina(25);
				opposingAthlete.useStamina(75);
			}
			else
			{
				opposingTeamScore += 100;
				teamAthlete.useStamina(75);
				opposingAthlete.useStamina(25);
			}
		}
	}
	
	/**
	   * Random Chance that the snitch mode is possible to play
	   */
	
	public void chanceSnitchOccurs(Athlete teamAthlete, Athlete opposingAthlete)
	{
		SplittableRandom random = new SplittableRandom();
		boolean snitchChance = random.nextInt(10) == 0;
		if (snitchChance == true)
		{
			snitchHeadToHead (teamAthlete, opposingAthlete);
		} else {
			teamAthlete.setStam(teamAthlete.getStamina() - 20);
		}
	}
	
	/**
	   * Instance of a bye Week and how it affects the stamina of the players. Also random chance of an increased stat of anywhere from 1-6.
	   */
	
	public void byeWeek()
	{
		for (Athlete byeWeekPlayer: user.getAthletes())
		{
			byeWeekPlayer.replenishStamina();
		}
		SplittableRandom random = new SplittableRandom();game.getAthletes();
		boolean number10Percentage = random.nextInt(10) == 0;
		if (number10Percentage == true)
		{
			int randomStatIncrease = random.nextInt(1, 6);
			int randomPlayerIndex = random.nextInt(user.getAthletes().size() - 1);
			user.getAthletes().get(randomPlayerIndex).addPotion();
		}
	}
	
	/**
	   * Random Chance that an athlete quits that is increased by 4 times if stamina is zero.
	   */
	
	public void chancePlayerQuits()
	{
		SplittableRandom random = new SplittableRandom();
		for (Athlete athleteAtRisk : user.getAthletes() )
		{
			if (athleteAtRisk.getStamina() == 0)
			{
				boolean injuredChance = random.nextInt(100) <= 3;
				if (injuredChance == true)
				{
					user.removeAthlete(athleteAtRisk);
				}
			}
			else if(athleteAtRisk.getStamina() > 0)
			{
				boolean notInjuredChance = random.nextInt(100) == 0;
				if (notInjuredChance == true)
				{
					user.removeAthlete(athleteAtRisk);
				}
			}
		}
	}
	
	/**
	   * Random Chance that an athlete joins that is doubled for every spot available in the team.
	   */
	
	/*public void chancePlayerJoins()
	{
		SplittableRandom random = new SplittableRandom();
		int spotsAvailable = 8 - user.getAthletes().size();
		int percentagePerSpot = 2 * spotsAvailable;
		if (spotsAvailable > 0)
		{
			boolean addPlayerChance = random.nextInt(100) < percentagePerSpot;
			if (addPlayerChance == true)
			{
				Athlete athleteJoining = athlete.createAhlete();
				user.addAthlete(athleteJoining);
			}
		}
	}*/
	
	
}


